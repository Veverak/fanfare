# coding=utf8
import os

def main():
    output = (
        "<title>Fanfare</title>"
        "<style>"
        "body{font-family:sans-serif;margin:auto;max-width:36em;padding:1em}"
        "</style>"
        "<h1>Fanfare and related packages</h1>"
    )
    toc = [
        "Overview",
        "Sound design",
        "Music Synthesizer for Android",
        "Music Synthesizer for Android – Usage",
        "Music Synthesizer for Android – Documentation",
        "DX7 reference implementation",
        "Synthesizer generator",
        "Synthesizer",
        "Fanfare",
    ]
    for name in toc:
        output += "<p><a href=\"" + name + ".html\">" + name + "</a></p>"
    print(output)

if __name__ == "__main__":
    main()
