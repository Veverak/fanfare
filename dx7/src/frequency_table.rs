use std::mem::MaybeUninit;

const lg_sample_count: i32 = 10;
const sample_count: i32 = 1 << lg_sample_count;
const sample_shift: i32 = 24 - lg_sample_count;
const max_log_frequency_int: i32 = 20;

static mut table: [MaybeUninit<i32>; (sample_count + 1) as usize] = [MaybeUninit::uninit(); (sample_count + 1) as usize];

pub unsafe fn init(sample_rate: f64) {
    let mut value = (1i64 << (max_log_frequency_int + 24)) as f64 / sample_rate;
    let step_ratio = 2f64.powf(1. / sample_count as f64);
    for i in 0..=sample_count {
        table[i as usize] = MaybeUninit::new(value.round() as _);
        value *= step_ratio;
    }
}

pub fn lookup(initialized: crate::initialize::Initialized, log_frequency: i32) -> i32 {
    let table_index = (log_frequency & 0xffffff) >> sample_shift;
    let table_value1 = unsafe { table[table_index as usize].assume_init() };
    let table_value2 = unsafe { table[(table_index + 1) as usize].assume_init() };
    let low_bits = log_frequency & ((1 << sample_shift) - 1);
    let fractional_value = (table_value1 as i64 + ((((table_value2 - table_value1) as i64 * low_bits as i64)) >> sample_shift)) as i32;
    let high_bits = log_frequency >> 24;
    fractional_value >> (max_log_frequency_int - high_bits)
}
