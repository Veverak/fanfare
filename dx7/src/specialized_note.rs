#[derive(Default)]
pub struct Note {
    down: bool,
    feedback_buffer: [i32; 2],
    oscillators: [OscillatorState; 6],
}

#[derive(Default)]
struct OscillatorState {
    envelope: crate::specialized_envelope::Envelope,
    frequency: i32,
    gain_out: i32,
    phase: i32,
}

struct Oscillator {
    detune: i32,
    levels: [u8; 4],
    out_level: i32,
    rate_scaling_mask: i8,
    rates: [u8; 4],
}

const feedback: i32 = 6;

const feedback_bit_depth: i32 = 8;

const oscillators: [Oscillator; 6] = [
    Oscillator {
        detune: 0,
        levels: [63, 63, 0, 0],
        out_level: 3232,
        rate_scaling_mask: 0,
        rates: [63, 59, 18, 31],
    },
    Oscillator {
        detune: 240842,
        levels: [63, 49, 0, 0],
        out_level: 4064,
        rate_scaling_mask: 0,
        rates: [63, 16, 14, 32],
    },
    Oscillator {
        detune: -88242,
        levels: [63, 63, 0, 0],
        out_level: 3264,
        rate_scaling_mask: 0,
        rates: [63, 41, 22, 32],
    },
    Oscillator {
        detune: -88242,
        levels: [63, 49, 0, 0],
        out_level: 4064,
        rate_scaling_mask: 0,
        rates: [63, 16, 14, 32],
    },
    Oscillator {
        detune: 63788574,
        levels: [63, 49, 0, 0],
        out_level: 3680,
        rate_scaling_mask: -1,
        rates: [62, 29, 22, 38],
    },
    Oscillator {
        detune: -50424,
        levels: [63, 51, 0, 0],
        out_level: 4064,
        rate_scaling_mask: 0,
        rates: [63, 16, 14, 32],
    },
];

const rate_scaling_sensitivity: i32 = 3;

fn scale_rate(midi_note: i32) -> i32 {
    let x = (midi_note / 3 - 7).max(0).min(31);
    (rate_scaling_sensitivity * x) >> 3
}

impl Note {
    pub fn init(&mut self, initialized: crate::initialize::Initialized, midi_note: u8) {
        self.down = true;
        let note_frequency = crate::tuning::midi_note_to_log_frequency(initialized, midi_note as _);
        let rate_scaling = scale_rate(midi_note as _);
        for (oscillator_index, oscillator) in oscillators.iter().enumerate() {
            let oscillator_state = &mut self.oscillators[oscillator_index];
            oscillator_state.envelope.init(
                oscillator.rates,
                oscillator.levels,
                oscillator.out_level,
                oscillator.rate_scaling_mask as i32 & rate_scaling,
            );
            let frequency = note_frequency + oscillator.detune;
            oscillator_state.frequency = crate::frequency_table::lookup(initialized, frequency);
        }
    }

    pub fn compute(
        &mut self,
        initialized: crate::initialize::Initialized,
        output: &mut crate::Buffer,
        buffer: &mut crate::Buffer,
    ) {
        let k_level_thresh = 1120;
        for (oscillator_index, oscillator_state) in self.oscillators.iter_mut().enumerate() {
            let level_in = oscillator_state.envelope.get_sample(self.down);
            let gain1 = oscillator_state.gain_out;
            let gain2 = crate::exp2::lookup(initialized, level_in - 14 * (1 << 24));
            oscillator_state.gain_out = gain2;
            if gain1 >= k_level_thresh || gain2 >= k_level_thresh || oscillator_index & 1 == 0 {
                if oscillator_index == 0 {
                    compute_feedback(initialized, buffer, oscillator_state.phase, oscillator_state.frequency, gain1, gain2, &mut self.feedback_buffer);
                } else if oscillator_index & 1 > 0 {
                    compute(initialized, output, buffer, oscillator_state.phase, oscillator_state.frequency, gain1, gain2);
                } else {
                    compute_pure(initialized, buffer, oscillator_state.phase, oscillator_state.frequency, gain1, gain2);
                }
            }
            oscillator_state.phase = oscillator_state.phase.wrapping_add(oscillator_state.frequency << crate::lg_buffer_size);
        }
    }

    pub fn key_up(&mut self, initialized: crate::initialize::Initialized) {
        self.down = false;
        for oscillator_state in &mut self.oscillators {
            oscillator_state.envelope.key_up();
        }
    }
}

pub fn compute(
    initialized: crate::initialize::Initialized,
    output: &mut crate::Buffer,
    input: &[i32],
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    for sample_index in 0..crate::buffer_size {
        gain += dgain;
        let y = crate::sin::lookup(initialized, phase.wrapping_add(input[sample_index]));
        output[sample_index] += ((y as i64 * gain as i64) >> 24) as i32;
        phase = phase.wrapping_add(frequency);
    }
}

pub fn compute_feedback(
    initialized: crate::initialize::Initialized,
    output: &mut crate::Buffer,
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
    feedback_buffer: &mut [i32; 2],
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    let mut y0 = feedback_buffer[0];
    let mut y = feedback_buffer[1];
    for sample_index in 0..crate::buffer_size {
        gain += dgain;
        let scaled_feedback = (y0 + y) >> (feedback_bit_depth - feedback + 1);
        y0 = y;
        y = crate::sin::lookup(initialized, phase.wrapping_add(scaled_feedback));
        y = ((y as i64 * gain as i64) >> 24) as _;
        output[sample_index] = y;
        phase = phase.wrapping_add(frequency);
    }
    *feedback_buffer = [y0, y];
}

pub fn compute_pure(
    initialized: crate::initialize::Initialized,
    output: &mut crate::Buffer,
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    for sample_index in 0..crate::buffer_size {
        gain += dgain;
        let y = crate::sin::lookup(initialized, phase);
        output[sample_index] = ((y as i64 * gain as i64) >> 24) as i32;
        phase = phase.wrapping_add(frequency);
    }
}
