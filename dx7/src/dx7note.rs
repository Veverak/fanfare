use std::convert::TryInto;

#[derive(Default)]
pub struct Dx7Note {
    pub mpe_pitch_bend: i32,
    pub mpe_pressure: i32,
    pub mpe_timbre: i32,
    envelope: [crate::envelope::Envelope; 6],
    params: [crate::fm_op_kernel::FmOpParams; 6],
    pitch_envelope: crate::pitch_envelope::PitchEnvelope,
    base_pitch: [i32; 6],
    feedback_buffer: [i32; 2],
    feedback_shift: i32,
    amp_mod_sens: [i32; 6],
    op_mode: [i32; 6],
    amp_mod_depth: i32,
    algorithm: [u8; 6],
    pitch_mod_depth: i32,
    pitch_mod_sens: i32,
}

pub struct VoiceStatus {
    amp: [u32; 6],
    amp_step: [u8; 6],
    pitch_step: u8,
}

const amp_mod_sens_table: [i32; 4] = [0, 4342338, 7171437, 16777216];

pub const coarse_table: [i32; 32] = [
    -16777216, 0, 16777216, 26591258, 33554432, 38955489, 43368474, 47099600,
    50331648, 53182516, 55732705, 58039632, 60145690, 62083076, 63876816,
    65546747, 67108864, 68576247, 69959732, 71268397, 72509921, 73690858,
    74816848, 75892776, 76922906, 77910978, 78860292, 79773775, 80654032,
    81503396, 82323963, 83117622,
];

const exp_scale_data: [u8; 33] = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 14, 16, 19, 23, 27, 33, 39, 47, 56, 66,
    80, 94, 110, 126, 142, 158, 174, 190, 206, 222, 238, 250,
];

const feedback_bit_depth: i32 = 8;

const pitch_mod_sens_table: [u8; 8] = [0, 10, 20, 33, 55, 92, 153, 255];

const super_precise: bool = false;

const velocity_data: [u8; 64] = [
    0, 70, 86, 97, 106, 114, 121, 126, 132, 138, 142, 148, 152, 156, 160, 163,
    166, 170, 173, 174, 178, 181, 184, 186, 189, 190, 194, 196, 198, 200, 202,
    205, 206, 209, 211, 214, 216, 218, 220, 222, 224, 225, 227, 229, 230, 232,
    233, 235, 237, 238, 240, 241, 242, 243, 244, 246, 246, 248, 249, 250, 251,
    252, 253, 254,
];

fn oscillator_frequency(
    initialized: crate::initialize::Initialized,
    midi_note: i32,
    mode: i32,
    coarse: i32,
    fine: i32,
    detune: i32,
) -> i32 {
    if mode == 0 {
        let mut log_frequency = crate::tuning::midi_note_to_log_frequency(initialized, midi_note);
        if crate::dexed {
            let detune_ratio = 0.0209 * (-0.396 * (log_frequency as f32 / (1 << 24) as f32)).exp() as f64 / 7.;
            log_frequency = (log_frequency as f64 + detune_ratio * log_frequency as f64 * (detune - 7) as f64) as i32;
        }
        log_frequency += coarse_table[(coarse & 31) as usize];
        if !crate::dexed {
            log_frequency += 12606 * (detune - 7);
        }
        if fine == 0 {
            log_frequency
        } else {
            log_frequency + ((1 << 24) as f64 / std::f64::consts::LN_2 * ((fine + 100) as f64 / 100.).ln() + 0.5).floor() as i32
        }
    } else {
        let log_frequency = (4458616 * ((coarse & 3) * 100 + fine)) >> 3;
        if detune > 7 {
            log_frequency + 13457 * (detune - 7)
        } else {
            log_frequency
        }
    }
}

fn scale_curve(group: i32, depth: u8, curve: u8) -> i32 {
    let scale = if matches!(curve, 0 | 3) {
        (group * depth as i32 * 329) >> 12
    } else {
        let raw_exp = exp_scale_data[(group as usize).min(exp_scale_data.len() - 1)];
        (raw_exp as i32 * depth as i32 * 329) >> 15
    };
    if curve < 2 {
        -scale
    } else {
        scale
    }
}

fn scale_level(midi_note: u8, break_point: u8, left_depth: u8, right_depth: u8, left_curve: u8, right_curve: u8) -> i32 {
    let offset = midi_note as i32 - break_point as i32 - 17;
    if offset < 0 {
        if crate::dexed {
            scale_curve(-(offset - 1) / 3, left_depth, left_curve)
        } else {
            scale_curve((-offset) / 3, left_depth, left_curve)
        }
    } else {
        if crate::dexed {
            scale_curve((offset + 1) / 3, right_depth, right_curve)
        } else {
            scale_curve(offset / 3, right_depth, right_curve)
        }
    }
}

fn scale_velocity(velocity: i32, sensitivity: i32) -> i32 {
    let clamped_velocity = velocity.min(127).max(0);
    let velocity_value = velocity_data[(clamped_velocity >> 1) as usize] as i32 - 239;
    ((sensitivity * velocity_value + 7) >> 3) << 4
}

fn scale_rate(midi_note: i32, sensitivity: i32) -> i32 {
    let x = (midi_note / 3 - 7).max(0).min(31);
    let qrate_delta = (sensitivity * x) >> 3;
    if super_precise {
        let rem = x & 7;
        if sensitivity == 3 && rem == 3 {
            qrate_delta - 1
        } else if sensitivity == 7 && rem > 0 && rem < 4 {
            qrate_delta + 1
        } else {
            qrate_delta
        }
    } else {
        qrate_delta
    }
}

impl Dx7Note {
    #[cfg(any(feature = "dexed", feature = "music_synthesizer_for_android", feature = "webdx7"))]
    pub fn dump(&self) -> crate::legacy::NoteDump {
        crate::legacy::NoteDump {
            envelope: self.envelope.iter().map(|envelope| envelope.dump()).collect::<Vec<_>>().try_into().unwrap(),
            pitch_envelope: self.pitch_envelope.dump(),
            base_pitch: self.base_pitch,
            feedback_buffer: self.feedback_buffer,
            feedback_shift: self.feedback_shift,
            pitch_mod_depth: self.pitch_mod_depth,
            pitch_mod_sens: self.pitch_mod_sens,
        }
    }

    pub fn init(&mut self, initialized: crate::initialize::Initialized, patch: &[u8; 156], midi_note: u8, velocity: i32) {
        for operator_index in 0..6 {
            let offset = 21 * operator_index;
            let rates = [patch[offset] as _, patch[offset + 1] as _, patch[offset + 2] as _, patch[offset + 3] as _];
            let levels = [patch[offset + 4] as _, patch[offset + 5] as _, patch[offset + 6] as _, patch[offset + 7] as _];
            let out_level = crate::envelope::scale_out_level(patch[offset + 16] as _);
            let level_scaling = scale_level(midi_note, patch[offset + 8], patch[offset + 9], patch[offset + 10], patch[offset + 11], patch[offset + 12]);
            let out_level = out_level + level_scaling;
            let out_level = out_level.min(127);
            let out_level = out_level << 5;
            let out_level = out_level + scale_velocity(velocity, patch[offset + 15] as _);
            let out_level = out_level.max(0);
            let rate_scaling = scale_rate(midi_note as _, patch[offset + 13] as _);
            self.envelope[operator_index].init(rates, levels, out_level, rate_scaling);
            let mode = patch[offset + 17];
            let coarse = patch[offset + 18];
            let fine = patch[offset + 19];
            let detune = patch[offset + 20];
            let frequency = oscillator_frequency(initialized, midi_note as _, mode as _, coarse as _, fine as _, detune as _);
            self.op_mode[operator_index] = mode as _;
            self.base_pitch[operator_index] = frequency;
            self.amp_mod_sens[operator_index] = amp_mod_sens_table[(patch[offset + 14] & 3) as usize];
        }
        let rates = [patch[126] as _, patch[126 + 1] as _, patch[126 + 2] as _, patch[126 + 3] as _];
        let levels = [patch[126 + 4] as _, patch[126 + 5] as _, patch[126 + 6] as _, patch[126 + 7] as _];
        self.pitch_envelope.set(initialized, rates, levels);
        self.algorithm = crate::fm_core::algorithms[patch[134] as usize];
        let feedback = patch[135];
        self.feedback_shift = if feedback == 0 {
            16
        } else {
            feedback_bit_depth - feedback as i32
        };
        self.pitch_mod_depth = (patch[139] as i32 * 165) >> 6;
        self.pitch_mod_sens = pitch_mod_sens_table[(patch[143] & 7) as usize] as _;
        self.amp_mod_depth = (patch[140] as i32 * 165) >> 6;
        self.mpe_pitch_bend = 8192;
        self.mpe_timbre = 0;
        self.mpe_pressure = 0;
    }

    pub fn compute(
        &mut self,
        initialized: crate::initialize::Initialized,
        output: &mut [i32; crate::buffer_size],
        lfo_val: i32,
        lfo_delay: i32,
        controllers: &mut crate::controllers::Controllers,
    ) {
        let pmd = self.pitch_mod_depth as u32 * lfo_delay as u32;
        let sens_lfo = self.pitch_mod_sens * (lfo_val - (1 << 23));
        let pmod_1 = ((pmd as i64 * sens_lfo as i64) >> 39).abs() as i32;
        let pmod_2 = ((controllers.pitch_mod as i64 * sens_lfo as i64) >> 14) as i32;
        let pitch_mod = pmod_1.max(pmod_2);
        let pitch_mod = self.pitch_envelope.get_sample(initialized) + if sens_lfo < 0 { -pitch_mod } else { pitch_mod };

        let pitch_bend = controllers.values[crate::controllers::k_controller_pitch];
        let mut pb = pitch_bend - 0x2000;
        if pb != 0 {
            if controllers.values[crate::controllers::k_controller_pitch_step] == 0 {
                let stp = 12 / controllers.values[crate::controllers::k_controller_pitch_step];
                pb = pb * stp / 8191;
                pb = (8191 / stp * pb) << 11;
            } else if pb < 0 {
                pb = ((pb << 11) as f64 * controllers.values[crate::controllers::k_controller_pitch_range_down] as f64 / 12.) as _;
            } else {
                pb = ((pb << 11) as f64 * controllers.values[crate::controllers::k_controller_pitch_range_up] as f64 / 12.) as _;
            }
        }

        if controllers.mpe_enabled {
            pb += (((self.mpe_pitch_bend - 0x2000) << 11) as f64 * controllers.mpe_pitch_bend_range as f64 / 12.) as i32;
        }

        let pitch_base = pb + controllers.master_tune;
        let pitch_mod = pitch_mod + pitch_base;

        let lfo_val = (1 << 24) - lfo_val;
        let amod_1 = ((self.amp_mod_depth as i64 * lfo_delay as i64) >> 8) as u32;
        let amod_1 = ((amod_1 as i64 * lfo_val as i64) >> 24) as u32;
        let amod_2 = ((controllers.amp_mod as i64 * lfo_val as i64) >> 7) as u32;
        let amd_mod = amod_1.max(amod_2);

        let amod_3 = (controllers.eg_mod + 1) << 17;
        let amd_mod = amd_mod.max((1 << 24) - amod_3 as u32);

        for op in 0..6 {
            if controllers.op_switch[op] == b'0' {
                self.envelope[op].get_sample();
                self.params[op].level_in = 0;
            } else {
                let pitch = if crate::dexed && self.op_mode[op] != 0 {
                    pitch_base
                } else {
                    pitch_mod
                };
                self.params[op].frequency = crate::frequency_table::lookup(initialized, self.base_pitch[op] + pitch);
                let mut level = self.envelope[op].get_sample();
                if crate::dexed && self.amp_mod_sens[op] != 0 {
                    let sens_amp = ((amd_mod as u64 * self.amp_mod_sens[op] as u64) >> 24) as u32;
                    let pt = (sens_amp as f64 / 262144. * 0.07 + 12.2).exp() as u32;
                    let ldiff = ((level as u64 * ((pt as u64) << 4)) >> 28) as i32;
                    level -= ldiff;
                }
                self.params[op].level_in = level;
            }
        }

        controllers.core.render(initialized, output, &mut self.params, self.algorithm, &mut self.feedback_buffer, self.feedback_shift);
    }

    pub fn key_up(&mut self, initialized: crate::initialize::Initialized) {
        for envelope in &mut self.envelope {
            envelope.key_down(false);
        }
        self.pitch_envelope.key_down(initialized, false);
    }

    pub fn update(&mut self, initialized: crate::initialize::Initialized, patch: &[u8; 156], midi_note: u8, velocity: i32) {
        for operator in 0..6 {
            let offset = 21 * operator;
            let rates = [patch[offset] as _, patch[offset + 1] as _, patch[offset + 2] as _, patch[offset + 3] as _];
            let levels = [patch[offset + 4] as _, patch[offset + 5] as _, patch[offset + 6] as _, patch[offset + 7] as _];
            let out_level = crate::envelope::scale_out_level(patch[offset + 16] as _);
            let level_scaling = scale_level(midi_note, patch[offset + 8], patch[offset + 9], patch[offset + 10], patch[offset + 11], patch[offset + 12]);
            let out_level = out_level + level_scaling;
            let out_level = out_level.min(127);
            let out_level = out_level << 5;
            let out_level = out_level + scale_velocity(velocity, patch[offset + 15] as _);
            let out_level = out_level.max(0);
            let rate_scaling = scale_rate(midi_note as _, patch[offset + 13] as _);
            self.envelope[operator].update(rates, levels, out_level, rate_scaling);
            let mode = patch[offset + 17];
            let coarse = patch[offset + 18];
            let fine = patch[offset + 19];
            let detune = patch[offset + 20];
            let frequency = oscillator_frequency(initialized, midi_note as _, mode as _, coarse as _, fine as _, detune as _);
            self.op_mode[operator] = mode as _;
            self.base_pitch[operator] = frequency;
            self.amp_mod_sens[operator] = amp_mod_sens_table[(patch[offset + 14] & 3) as usize];
        }
        self.algorithm = crate::fm_core::algorithms[patch[134] as usize];
        let feedback = patch[135];
        self.feedback_shift = if feedback == 0 {
            16
        } else {
            feedback_bit_depth - feedback as i32
        };
        self.pitch_mod_depth = (patch[139] as i32 * 165) >> 6;
        self.pitch_mod_sens = pitch_mod_sens_table[(patch[143] & 7) as usize] as _;
        self.amp_mod_depth = (patch[140] as i32 * 165) >> 6;
    }

    pub fn peek_voice_status(&self) -> VoiceStatus {
        todo!();
    }

    pub fn transfer_state(&mut self, src: &Dx7Note) {
        self.envelope = src.envelope;
        self.transfer_signal(src);
    }

    pub fn transfer_signal(&mut self, src: &Dx7Note) {
        for (target_param, source_param) in self.params.iter_mut().zip(src.params.iter()) {
            target_param.gain_out = source_param.gain_out;
            target_param.phase = source_param.phase;
        }
    }

    pub fn osc_sync(&mut self) {
        for param in &mut self.params {
            param.gain_out = 0;
            param.phase = 0;
        }
    }
}
