#![allow(non_upper_case_globals)]
#![forbid(non_ascii_idents)]

mod controllers;
mod dx7note;
mod envelope;
mod exp2;
mod fm_core;
mod fm_op_kernel;
mod frequency_table;
mod initialize;
#[cfg(any(feature = "dexed", feature = "music_synthesizer_for_android", feature = "webdx7"))]
mod legacy;
mod lfo;
mod load;
mod pitch_envelope;
mod sin;
mod specialized_envelope;
mod specialized_note;
mod tanh;
mod tuning;
mod unpack;

use std::convert::TryInto;

const dexed: bool = cfg!(feature = "dexed");
const lg_buffer_size: i32 = 6;
const buffer_size: usize = (1 << lg_buffer_size) as usize;

const header: &[u8] = &[0xf0, 0x43, 0x00, 0x09, 0x20, 0x00];

type Buffer = [i32; buffer_size];

fn main() {
    let mut arguments = std::env::args();
    match &arguments.nth(1).unwrap_or_default()[..] {
        "find" => {
            if arguments.len() != 1 {
                invalid_use();
            }
            let bank_file_path = arguments.next().unwrap();
            find(&bank_file_path);
        }
        "test" => {
            if arguments.len() != 1 {
                invalid_use();
            }
            let bank_file_path = arguments.next().unwrap();
            #[cfg(any(feature = "dexed", feature = "music_synthesizer_for_android", feature = "webdx7"))]
            {
                let initialized = unsafe { initialize::initialize() };
                legacy::test(initialized, &bank_file_path);
            }
            #[cfg(not(any(feature = "dexed", feature = "music_synthesizer_for_android", feature = "webdx7")))]
            exit("the feature is not enabled");
        }
        "play" => {
            if arguments.len() != 3 {
                invalid_use();
            }
            let bank_file_path = arguments.next().unwrap();
            let patch_index = arguments.next().unwrap().parse().ok().unwrap_or_else(|| {
                invalid_use();
            });
            let output_file_path = arguments.next().unwrap();
            play(&bank_file_path, patch_index, &output_file_path);
        }
        "play_specialized" => {
            if arguments.len() != 1 {
                invalid_use();
            }
            let output_file_path = arguments.next().unwrap();
            play_specialized(&output_file_path);
        }
        "verify_specialized" => {
            if cfg!(feature = "compatibility_with_specialized") {
                if arguments.len() != 1 {
                    invalid_use();
                }
                let bank_file_path = arguments.next().unwrap();
                verify_specialized(&bank_file_path);
            } else {
                exit("the feature is not enabled");
            }
        }
        _ => {
            invalid_use();
        }
    }
}

fn exit(message: &str) -> ! {
    eprintln!("{}", message);
    std::process::exit(1);
}

fn find(bank_file_path: &str) {
    let bank = std::fs::read(bank_file_path).unwrap();
    if bank.len() == 4104 {
        if !bank.starts_with(header) {
            exit("unrecognized bank format");
        }
        for patch_index in 0..32 {
            let patch_offset = header.len() + 128 * patch_index as usize;
            let source = bank[patch_offset..patch_offset + 128].try_into().unwrap();
            let patch = unpack::unpack_patch(&source).unwrap();
            eprintln!(
                "patch {}, name: {:?}, pitch envelope levels: {}, {}, {}, {}",
                patch_index,
                std::str::from_utf8(&patch[145..155]).unwrap(),
                patch[126 + 4], patch[126 + 5], patch[126 + 6], patch[126 + 7],
            );
        }
    } else if bank.len() % 145 == 0 {
        for (patch_index, patch_source) in bank.chunks(145).enumerate() {
            eprintln!("{}", patch_index);
        }
    } else {
        exit("unrecognized bank format");
    }
}

fn invalid_use() -> ! {
    exit("invalid use");
}

fn play(bank_file_path: &str, patch_index: usize, output_file_path: &str) {
    if fm_core::compatibility_with_specialized {
        eprintln!("warning: Compatibility with specialized should be disabled.");
    }
    let patch = load::load_patch(bank_file_path, patch_index);
    let initialized = unsafe { initialize::initialize() };
    let midi_note = 64;
    let velocity = 127;
    let mut controllers = controllers::Controllers::default();
    controllers.refresh();
    let mut lfo = lfo::Lfo::default();
    lfo.reset(initialized, patch[137..137 + 6].try_into().unwrap());
    let mut note1 = dx7note::Dx7Note::default();
    note1.init(initialized, &patch, midi_note, velocity);
    let mut note2 = dx7note::Dx7Note::default();
    note2.init(initialized, &patch, midi_note + 7, velocity);
    let mut output = [0; buffer_size];
    let mut file_contents = vec![];
    let sample_rate = 44100;
    let duration = 1000;
    let byte_rate = 2 * sample_rate;
    let data_size = 2 * buffer_size * duration;
    let file_size = data_size + 36;
    file_contents.extend_from_slice(&[
        b'R', b'I', b'F', b'F',
        file_size as _, (file_size >> 8) as _, (file_size >> 16) as _, (file_size >> 24) as _,
        b'W', b'A', b'V', b'E',
        b'f', b'm', b't', b' ',
        16, 0, 0, 0,
        1, 0,
        1, 0,
        sample_rate as _, (sample_rate >> 8) as _, (sample_rate >> 16) as _, (sample_rate >> 24) as _,
        byte_rate as _, (byte_rate >> 8) as _, (byte_rate >> 16) as _, (byte_rate >> 24) as _,
        2, 0,
        16, 0,
        b'd', b'a', b't', b'a',
        data_size as _, (data_size >> 8) as _, (data_size >> 16) as _, (data_size >> 24) as _,
    ]);
    for progress in 0..duration {
        match progress {
            50 => {
                note1.key_up(initialized);
            }
            100 => {
                note2.key_up(initialized);
            }
            _ => {}
        }
        let lfo_value = lfo.get_sample(initialized);
        let lfo_delay = lfo.get_delay();
        output.fill(0);
        note1.compute(initialized, &mut output, lfo_value, lfo_delay, &mut controllers);
        if progress >= 50 {
            note2.compute(initialized, &mut output, lfo_value, lfo_delay, &mut controllers);
        }
        for &sample in &output {
            file_contents.push((sample >> 13) as _);
            file_contents.push((sample >> 21) as _);
        }
    }
    std::fs::write(output_file_path, file_contents).unwrap();
}

fn play_specialized(output_file_path: &str) {
    let initialized = unsafe { initialize::initialize() };
    let midi_note = 64;
    let mut buffer = [0; buffer_size];
    let mut note1 = specialized_note::Note::default();
    note1.init(initialized, midi_note);
    let mut note2 = specialized_note::Note::default();
    note2.init(initialized, midi_note + 7);
    let mut output = [0; buffer_size];
    let mut file_contents = vec![];
    let sample_rate = 44100;
    let duration = 1000;
    let byte_rate = 2 * sample_rate;
    let data_size = 2 * buffer_size * duration;
    let file_size = data_size + 36;
    file_contents.extend_from_slice(&[
        b'R', b'I', b'F', b'F',
        file_size as _, (file_size >> 8) as _, (file_size >> 16) as _, (file_size >> 24) as _,
        b'W', b'A', b'V', b'E',
        b'f', b'm', b't', b' ',
        16, 0, 0, 0,
        1, 0,
        1, 0,
        sample_rate as _, (sample_rate >> 8) as _, (sample_rate >> 16) as _, (sample_rate >> 24) as _,
        byte_rate as _, (byte_rate >> 8) as _, (byte_rate >> 16) as _, (byte_rate >> 24) as _,
        2, 0,
        16, 0,
        b'd', b'a', b't', b'a',
        data_size as _, (data_size >> 8) as _, (data_size >> 16) as _, (data_size >> 24) as _,
    ]);
    for progress in 0..duration {
        match progress {
            50 => {
                note1.key_up(initialized);
            }
            100 => {
                note2.key_up(initialized);
            }
            _ => {}
        }
        output.fill(0);
        note1.compute(initialized, &mut output, &mut buffer);
        if progress >= 50 {
            note2.compute(initialized, &mut output, &mut buffer);
        }
        for &sample in &output {
            file_contents.push((sample >> 13) as _);
            file_contents.push((sample >> 21) as _);
        }
    }
    std::fs::write(output_file_path, file_contents).unwrap();
}

fn verify_specialized(bank_file_path: &str) {
    let patch = load::load_patch(bank_file_path, 5032);

    let initialized = unsafe { initialize::initialize() };
    let midi_note = 64;
    let velocity = 127;

    let mut controllers = controllers::Controllers::default();
    controllers.refresh();
    let mut lfo = lfo::Lfo::default();
    lfo.reset(initialized, patch[137..137 + 6].try_into().unwrap());
    let mut dx7_note1 = dx7note::Dx7Note::default();
    dx7_note1.init(initialized, &patch, midi_note, velocity);
    let mut dx7_note2 = dx7note::Dx7Note::default();
    dx7_note2.init(initialized, &patch, midi_note + 7, velocity);

    let mut buffer = [0; buffer_size];
    let mut specialized_note1 = specialized_note::Note::default();
    specialized_note1.init(initialized, midi_note);
    let mut specialized_note2 = specialized_note::Note::default();
    specialized_note2.init(initialized, midi_note + 7);
    let mut dx7_output = [0; buffer_size];
    let mut specialized_output = [0; buffer_size];
    let duration = 10000;
    for progress in 0..duration {
        match progress {
            50 => {
                dx7_note1.key_up(initialized);
                specialized_note1.key_up(initialized);
            }
            100 => {
                dx7_note2.key_up(initialized);
                specialized_note2.key_up(initialized);
            }
            _ => {}
        }

        let lfo_value = lfo.get_sample(initialized);
        let lfo_delay = lfo.get_delay();
        dx7_output.fill(0);
        dx7_note1.compute(initialized, &mut dx7_output, lfo_value, lfo_delay, &mut controllers);
        if progress >= 50 {
            dx7_note2.compute(initialized, &mut dx7_output, lfo_value, lfo_delay, &mut controllers);
        }

        specialized_output.fill(0);
        specialized_note1.compute(initialized, &mut specialized_output, &mut buffer);
        if progress >= 50 {
            specialized_note2.compute(initialized, &mut specialized_output, &mut buffer);
        }

        assert_eq!(dx7_output, specialized_output, "progress {}", progress);
    }
}
