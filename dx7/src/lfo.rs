#[derive(Default)]
pub struct Lfo {
    phase: u32,
    delta: u32,
    waveform: u8,
    random_state: u8,
    sync: bool,
    delay_state: u32,
    delay_increment1: u32,
    delay_increment2: u32,
}

static mut unit: u32 = 0;

impl Lfo {
    #[cfg(any(feature = "dexed", feature = "music_synthesizer_for_android", feature = "webdx7"))]
    pub fn dump(&self) -> crate::legacy::LfoDump {
        crate::legacy::LfoDump {
            phase: self.phase,
            delta: self.delta,
            waveform: self.waveform,
            random_state: self.random_state,
            sync: self.sync,
            delay_state: self.delay_state,
            delay_increment1: self.delay_increment1,
            delay_increment2: self.delay_increment2,
        }
    }

    pub fn reset(&mut self, initialized: crate::initialize::Initialized, params: [u8; 6]) {
        let rate = params[0];
        let sr = if rate == 0 {
            1
        } else {
            (165 * rate as i32) >> 6
        };
        let sr = sr * if sr < 160 {
            11
        } else {
            11 + ((sr - 160) >> 4)
        };
        let local_unit = unsafe { unit };
        self.delta = local_unit * sr as u32;
        let a = 99 - params[1] as i32;
        if a == 99 {
            self.delay_increment1 = !0;
            self.delay_increment2 = !0;
        } else {
            let a = (16 + (a & 15)) << (1 + (a >> 4));
            self.delay_increment1 = local_unit * a as u32;
            let a = a & 0xff80;
            let a = a.max(0x80);
            self.delay_increment2 = local_unit * a as u32;
        }
        self.waveform = params[5];
        self.sync = params[4] != 0;
    }

    pub fn get_sample(&mut self, initialized: crate::initialize::Initialized) -> i32 {
        self.phase = self.phase.wrapping_add(self.delta);
        match self.waveform {
            0 => {
                let x = self.phase >> 7;
                let x = x as i32 ^ -((self.phase >> 31) as i32);
                x & ((1 << 24) - 1)
            }
            1 => ((!self.phase ^ (1 << 31)) >> 8) as _,
            2 => ((self.phase ^ (1 << 31)) >> 8) as _,
            3 => ((!self.phase >> 7) & (1 << 24)) as _,
            4 => (1 << 23) + (crate::sin::lookup(initialized, (self.phase >> 8) as i32) >> 1),
            5 => {
                if self.phase < self.delta {
                    self.random_state = ((self.random_state as u32).wrapping_mul(179).wrapping_add(17)) as _;
                }
                let x = self.random_state ^ 0x80;
                return (x as i32 + 1) << 16;
            }
            _ => 1 << 23,
        }
    }

    pub fn get_delay(&mut self) -> i32 {
        let delta = if self.delay_state < (1 << 31) {
            self.delay_increment1
        } else {
            self.delay_increment2
        };
        let d;
        if crate::dexed {
            match self.delay_state.checked_add(delta) {
                None => {
                    return 1 << 24;
                }
                Some(value) => {
                    d = value;
                }
            }
        } else {
            d = self.delay_state.wrapping_add(delta);
            if d < self.delay_increment1 {
                return 1 << 24;
            }
        }
        self.delay_state = d;
        if d < (1 << 31) {
            0
        } else {
            (d >> 7) as i32 & ((1 << 24) - 1)
        }
    }

    pub fn key_down(&mut self) {
        if self.sync {
            self.phase = (1 << 31) - 1;
        }
        self.delay_state = 0;
    }
}

pub unsafe fn init(sample_rate: f64) {
    unit = ((crate::buffer_size * 25190424) as f64 / sample_rate + 0.5) as _;
}
