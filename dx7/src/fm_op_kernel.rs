#[derive(Clone, Copy, Default)]
pub struct FmOpParams {
    pub level_in: i32,
    pub gain_out: i32,
    pub frequency: i32,
    pub phase: i32,
}

pub fn compute(
    initialized: crate::initialize::Initialized,
    output: &mut crate::Buffer,
    input: &[i32],
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
    add: bool,
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    if add {
        for i in 0..crate::buffer_size {
            gain += dgain;
            let y = crate::sin::lookup(initialized, phase.wrapping_add(input[i as usize]));
            output[i as usize] += ((y as i64 * gain as i64) >> 24) as i32;
            phase = phase.wrapping_add(frequency);
        }
    } else {
        for i in 0..crate::buffer_size {
            gain += dgain;
            let y = crate::sin::lookup(initialized, phase.wrapping_add(input[i as usize]));
            output[i as usize] = ((y as i64 * gain as i64) >> 24) as i32;
            phase = phase.wrapping_add(frequency);
        }
    }
}

pub fn compute_in_situ(
    initialized: crate::initialize::Initialized,
    buffer: &mut crate::Buffer,
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
    add: bool,
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    if add {
        for i in 0..crate::buffer_size {
            gain += dgain;
            let y = crate::sin::lookup(initialized, phase.wrapping_add(buffer[i as usize]));
            buffer[i as usize] += ((y as i64 * gain as i64) >> 24) as i32;
            phase = phase.wrapping_add(frequency);
        }
    } else {
        for i in 0..crate::buffer_size {
            gain += dgain;
            let y = crate::sin::lookup(initialized, phase.wrapping_add(buffer[i as usize]));
            buffer[i as usize] = ((y as i64 * gain as i64) >> 24) as i32;
            phase = phase.wrapping_add(frequency);
        }
    }
}

pub fn compute_pure(
    initialized: crate::initialize::Initialized,
    output: &mut crate::Buffer,
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
    add: bool,
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    if add {
        for i in 0..crate::buffer_size {
            gain += dgain;
            let y = crate::sin::lookup(initialized, phase);
            output[i as usize] += ((y as i64 * gain as i64) >> 24) as i32;
            phase = phase.wrapping_add(frequency);
        }
    } else {
        for i in 0..crate::buffer_size {
            gain += dgain;
            let y = crate::sin::lookup(initialized, phase);
            output[i as usize] = ((y as i64 * gain as i64) >> 24) as i32;
            phase = phase.wrapping_add(frequency);
        }
    }
}

pub fn compute_feedback(
    initialized: crate::initialize::Initialized,
    output: &mut crate::Buffer,
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
    feedback_buffer: &mut [i32; 2],
    feedback_shift: i32,
    add: bool,
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    let mut y0 = feedback_buffer[0];
    let mut y = feedback_buffer[1];
    if add {
        for i in 0..crate::buffer_size {
            gain += dgain;
            let scaled_feedback = (y0 + y) >> (feedback_shift + 1);
            y0 = y;
            y = crate::sin::lookup(initialized, phase.wrapping_add(scaled_feedback));
            y = ((y as i64 * gain as i64) >> 24) as _;
            output[i as usize] += y;
            phase = phase.wrapping_add(frequency);
        }
    } else {
        for i in 0..crate::buffer_size {
            gain += dgain;
            let scaled_feedback = (y0 + y) >> (feedback_shift + 1);
            y0 = y;
            y = crate::sin::lookup(initialized, phase.wrapping_add(scaled_feedback));
            y = ((y as i64 * gain as i64) >> 24) as _;
            output[i as usize] = y;
            phase = phase.wrapping_add(frequency);
        }
    }
    feedback_buffer[0] = y0;
    feedback_buffer[1] = y;
}
