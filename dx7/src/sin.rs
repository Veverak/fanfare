use std::mem::MaybeUninit;

const lg_sample_count: i32 = 10;
const sample_count: i32 = 1 << lg_sample_count;
const use_delta_table: bool = true;

static mut table: [MaybeUninit<i32>; sample_count as usize + 1] = [MaybeUninit::uninit(); sample_count as usize + 1];
static mut delta_table: [MaybeUninit<i32>; (sample_count as usize) << 1] = [MaybeUninit::uninit(); (sample_count as usize) << 1];

pub unsafe fn init() {
    const r: i64 = 1 << 29;
    let phase = 2. * std::f64::consts::PI / sample_count as f64;
    let c = (phase.cos() * (1 << 30) as f64 + 0.5).floor() as i32;
    let s = (phase.sin() * (1 << 30) as f64 + 0.5).floor() as i32;
    let mut u = 1 << 30;
    let mut v = 0;
    for i in 0..sample_count / 2 {
        if use_delta_table {
            delta_table[((i << 1) + 1) as usize] = MaybeUninit::new((v + 32) >> 6);
            delta_table[(((i + sample_count / 2) << 1) + 1) as usize] = MaybeUninit::new(-((v + 32) >> 6));
        } else {
            table[i as usize] = MaybeUninit::new((v + 32) >> 6);
            table[(i + sample_count / 2) as usize] = MaybeUninit::new(-((v + 32) >> 6));
        }
        let t = ((u as i64 * s as i64 + v as i64 * c as i64 + r) >> 30) as i32;
        u = ((u as i64 * c as i64 - v as i64 * s as i64 + r) >> 30) as i32;
        v = t;
    }
    if use_delta_table {
        for i in 0..sample_count - 1 {
            delta_table[(i << 1) as usize] = MaybeUninit::new(delta_table[((i << 1) + 3) as usize].assume_init() - delta_table[((i << 1) + 1) as usize].assume_init());
        }
        delta_table[((sample_count << 1) - 2) as usize] = MaybeUninit::new(-delta_table[((sample_count << 1) - 1) as usize].assume_init());
    } else {
        table[sample_count as usize] = MaybeUninit::new(0);
    }
}

pub fn lookup(initialized: crate::initialize::Initialized, phase: i32) -> i32 {
    const shift: i32 = 24 - lg_sample_count;
    let low_bits = phase & ((1 << shift) - 1);
    if use_delta_table {
        let phase_int = (phase >> (shift - 1)) & ((sample_count - 1) << 1);
        let dy = unsafe { delta_table[phase_int as usize].assume_init() };
        let y0 = unsafe { delta_table[(phase_int + 1) as usize].assume_init() };
        (y0 as i64 + ((dy as i64 * low_bits as i64) >> shift)) as i32
    } else {
        let phase_int = (phase >> shift) & (sample_count - 1);
        let y0 = unsafe { table[phase_int as usize].assume_init() };
        let y1 = unsafe { table[(phase_int + 1) as usize].assume_init() };
        (y0 as i64 + (((y1 - y0) as i64 * low_bits as i64) >> shift)) as i32
    }
}

pub fn compute_alternative(phase: i32) -> i32 {
    const c0: i64 = 1 << 24;
    const c1: i64 = 331121857 >> 2;
    const c2: i64 = 1084885537 >> 4;
    const c3: i64 = 1310449902 >> 6;
    let x = (phase & ((1 << 23) - 1)) - (1 << 22);
    let x2 = ((x as i64 * x as i64) >> 22) as i64;
    let x4 = ((x2 as i64 * x2 as i64) >> 24) as i64;
    let x6 = ((x2 as i64 * x4 as i64) >> 24) as i64;
    let y = c0 -
        ((c1 * x2 as i64) >> 24) +
        ((c2 * x4 as i64) >> 24) -
        ((c3 * x6 as i64) >> 24);
    y as i32 ^ -((phase >> 23) & 1)
}

pub fn compute(phase: i32) -> i32 {
    const c8_0: i64 = 16777216;
    const c8_2: i64 = -331168742;
    const c8_4: i64 = 1089453524;
    const c8_6: i64 = -1430910663;
    const c8_8: i64 = 950108533;
    let x = (phase & ((1 << 23) - 1)) - (1 << 22);
    let x2 = (x as i64 * x as i64) >> 16;
    let y = ((((((((((((c8_8
        * x2 as i64) >> 32) + c8_6)
        * x2 as i64) >> 32) + c8_4)
        * x2 as i64) >> 32) + c8_2)
        * x2 as i64) >> 32) + c8_0) as i32;
    y ^ -((phase >> 23) & 1)
}

pub fn compute10(phase: i32) -> i32 {
    const c10_0: i64 = 1 << 30;
    const c10_2: i64 = -1324675874;
    const c10_4: i64 = 1089501821;
    const c10_6: i64 = -1433689867;
    const c10_8: i64 = 1009356886;
    const c10_10: i64 = -421101352;
    let x = (phase & ((1 << 29) - 1)) - (1 << 28);
    let x2 = (x as i64 * x as i64) >> 26;
    let y = (((((((((((((((c10_10
        * x2 as i64) >> 34) + c10_8)
        * x2 as i64) >> 34) + c10_6)
        * x2 as i64) >> 34) + c10_4)
        * x2 as i64) >> 32) + c10_2)
        * x2 as i64) >> 30) + c10_0) as i32;
    y ^ -((phase >> 29) & 1)
}
