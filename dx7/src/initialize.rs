#[derive(Clone, Copy)]
pub struct Initialized(());

pub unsafe fn initialize() -> Initialized {
    crate::exp2::init();
    crate::frequency_table::init(44100.);
    crate::lfo::init(44100.);
    crate::pitch_envelope::init(44100.);
    crate::sin::init();
    crate::tanh::init();
    crate::tuning::init();
    Initialized(())
}
