use std::convert::TryInto;

#[derive(Clone, Copy)]
#[repr(transparent)]
struct Controllers(*mut std::ffi::c_void);

#[derive(Clone, Copy)]
#[repr(transparent)]
struct Lfo(*mut std::ffi::c_void);

#[derive(Clone, Copy)]
#[repr(transparent)]
struct Note(*mut std::ffi::c_void);

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct EnvelopeDump {
    pub rates: [i32; 4],
    pub levels: [i32; 4],
    pub out_level: i32,
    pub rate_scaling: i32,
    pub level: i32,
    pub target_level: i32,
    pub rising: bool,
    pub ix: i32,
    pub increment: i32,
    pub static_count: i32,
    pub down: bool,
}

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct LfoDump {
    pub phase: u32,
    pub delta: u32,
    pub waveform: u8,
    pub random_state: u8,
    pub sync: bool,
    pub delay_state: u32,
    pub delay_increment1: u32,
    pub delay_increment2: u32,
}

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct NoteDump {
    pub envelope: [EnvelopeDump; 6],
    pub pitch_envelope: PitchEnvelopeDump,
    pub base_pitch: [i32; 6],
    pub feedback_buffer: [i32; 2],
    pub feedback_shift: i32,
    pub pitch_mod_depth: i32,
    pub pitch_mod_sens: i32,
}

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct PitchEnvelopeDump {
    pub rates: [i32; 4],
    pub levels: [i32; 4],
    pub level: i32,
    pub target_level: i32,
    pub rising: bool,
    pub ix: i32,
    pub increment: i32,
    pub down: bool,
}

#[link(kind = "static", name = "synth")]
extern "C" {
    fn controllers_create() -> Controllers;

    fn exp2_lookup(x: i32) -> i32;

    fn frequency_lookup(log_frequency: i32) -> i32;

    fn init();

    fn lfo_create() -> Lfo;

    fn lfo_dump(lfo: Lfo) -> LfoDump;

    fn lfo_get_delay(lfo: Lfo) -> i32;

    fn lfo_get_sample(lfo: Lfo) -> i32;

    fn lfo_key_down(lfo: Lfo);

    fn lfo_reset(lfo: Lfo, params: &[u8; 6]);

    fn note_compute(note: Note, output: &mut [i32; crate::buffer_size], lfo_val: i32, lfo_delay: i32, controllers: Controllers);

    fn note_create() -> Note;

    fn note_dump(note: Note) -> NoteDump;

    fn note_init(note: Note, patch: &[u8; 156], midi_note: i32, velocity: i32);

    fn note_key_up(note: Note);

    fn sin_lookup(phase: i32) -> i32;

    fn tanh_lookup(x: i32) -> i32;

    #[cfg(not(feature = "dexed"))]
    fn unpack_patch(source: &[u8; 128], target: &mut [u8; 156]);
}

pub fn test(initialized: crate::initialize::Initialized, bank_file_path: &str) {
    unsafe { init() };
    for x in 0..1_000_000 {
        assert_eq!(unsafe { exp2_lookup(x) }, crate::exp2::lookup(initialized, x));
    }
    for log_frequency in 0..1_000_000 {
        assert_eq!(unsafe { frequency_lookup(log_frequency) }, crate::frequency_table::lookup(initialized, log_frequency));
    }
    for phase in 0..1_000_000 {
        assert_eq!(unsafe { sin_lookup(phase) }, crate::sin::lookup(initialized, phase));
    }
    for x in 0..1_000_000 {
        assert_eq!(unsafe { tanh_lookup(x) }, crate::tanh::lookup(initialized, x));
    }
    let bank = std::fs::read(bank_file_path).unwrap_or_else(|error| {
        eprintln!("Failed to load bank file: {}", error);
        std::process::exit(1);
    });
    assert!(bank.starts_with(crate::header));
    let mut controllers = crate::controllers::Controllers::default();
    controllers.refresh();
    let legacy_controllers = unsafe { controllers_create() };
    for patch_index in 0..32 {
        eprintln!("patch {}", patch_index);
        let patch_offset = crate::header.len() + 128 * patch_index as usize;
        let source = bank[patch_offset..patch_offset + 128].try_into().unwrap();
        let patch = crate::unpack::unpack_patch(&source).unwrap();
        #[cfg(not(feature = "dexed"))]
        {
            let mut legacy_patch1 = [0; 156];
            let mut legacy_patch2 = [255; 156];
            unsafe { unpack_patch(&source, &mut legacy_patch1) };
            unsafe { unpack_patch(&source, &mut legacy_patch2) };
            assert_eq!(patch, legacy_patch1);
            assert_eq!(patch, legacy_patch2);
        }
        let mut lfo = crate::lfo::Lfo::default();
        lfo.reset(initialized, patch[137..137 + 6].try_into().unwrap());
        let legacy_lfo = unsafe { lfo_create() };
        unsafe { lfo_reset(legacy_lfo, &patch[137..137 + 6].try_into().unwrap()) };
        for _ in 0..10000 {
            assert_eq!(lfo.dump(), unsafe { lfo_dump(legacy_lfo) });
            assert_eq!(lfo.get_sample(initialized), unsafe { lfo_get_sample(legacy_lfo) });
            assert_eq!(lfo.dump(), unsafe { lfo_dump(legacy_lfo) });
            assert_eq!(lfo.get_delay(), unsafe { lfo_get_delay(legacy_lfo) });
        }
        for midi_note in 50..76 {
            let mut note = crate::dx7note::Dx7Note::default();
            note.init(initialized, &patch, midi_note, 127);
            let legacy_note = unsafe { note_create() };
            unsafe { note_init(legacy_note, &patch, midi_note as _, 127) };
            lfo.reset(initialized, patch[137..137 + 6].try_into().unwrap());
            for progress in 0..1000 {
                if progress == 50 {
                    note.key_up(initialized);
                    unsafe { note_key_up(legacy_note) };
                }
                assert_eq!(note.dump(), unsafe { note_dump(legacy_note) });
                let lfo_value = lfo.get_sample(initialized);
                let lfo_delay = lfo.get_delay();
                let mut output = [0; crate::buffer_size];
                note.compute(initialized, &mut output, lfo_value, lfo_delay, &mut controllers);
                let mut legacy_output = [0; crate::buffer_size];
                unsafe { note_compute(legacy_note, &mut legacy_output, lfo_value, lfo_delay, legacy_controllers) };
                assert_eq!(output, legacy_output);
            }
        }
    }
}
