#[derive(Clone, Copy, Default)]
pub struct FmMod {
    range: i32,
    pitch: bool,
    amp: bool,
    eg: bool,
}

pub struct Controllers {
    pub values: [i32; 132],
    pub op_switch: [u8; 7],
    pub amp_mod: i32,
    pub pitch_mod: i32,
    pub eg_mod: i32,
    pub aftertouch_cc: i32,
    pub breath_cc: i32,
    pub foot_cc: i32,
    pub modwheel_cc: i32,
    pub master_tune: i32,
    pub transpose_12_as_scale: bool,
    pub mpe_enabled: bool,
    pub mpe_pitch_bend_range: i32,
    pub wheel: FmMod,
    pub foot: FmMod,
    pub breath: FmMod,
    pub at: FmMod,
    pub core: crate::fm_core::FmCore,
}

pub const k_controller_pitch: usize = 128;
pub const k_controller_pitch_range_up: usize = 129;
pub const k_controller_pitch_step: usize = 130;
pub const k_controller_pitch_range_down: usize = 131;

impl Controllers {
    fn apply_mod(&mut self, cc: i32, r#mod: FmMod) {
        let range = 0.01 * r#mod.range as f64;
        let total = (cc as f64 * range) as i32;
        if r#mod.amp {
            self.amp_mod = self.amp_mod.max(total);
        }
        if r#mod.pitch {
            self.pitch_mod = self.pitch_mod.max(total);
        }
        if r#mod.eg {
            self.eg_mod = self.eg_mod.max(total);
        }
    }

    pub fn refresh(&mut self) {
        self.amp_mod = 0;
        self.pitch_mod = 0;
        self.eg_mod = 0;
        self.apply_mod(self.modwheel_cc, self.wheel);
        self.apply_mod(self.breath_cc, self.breath);
        self.apply_mod(self.foot_cc, self.foot);
        self.apply_mod(self.aftertouch_cc, self.at);
        if !(self.wheel.eg || self.foot.eg || self.breath.eg || self.at.eg) {
            self.eg_mod = 127;
        }
    }
}

impl Default for Controllers {
    fn default() -> Self {
        let mut values = [0; 132];
        values[k_controller_pitch] = 0x2000;
        Controllers {
            values,
            op_switch: [0; 7],
            amp_mod: 0,
            pitch_mod: 0,
            eg_mod: 0,
            aftertouch_cc: 0,
            breath_cc: 0,
            foot_cc: 0,
            modwheel_cc: 0,
            master_tune: 0,
            transpose_12_as_scale: true,
            mpe_enabled: true,
            mpe_pitch_bend_range: 0,
            wheel: Default::default(),
            foot: Default::default(),
            breath: Default::default(),
            at: Default::default(),
            core: Default::default(),
        }
    }
}
