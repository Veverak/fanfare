use std::mem::MaybeUninit;

const lg_sample_count: i32 = 10;
const sample_count: i32 = 1 << lg_sample_count;

static mut exp2_table: [MaybeUninit<i32>; (sample_count << 1) as usize] = [MaybeUninit::uninit(); (sample_count << 1) as usize];

pub unsafe fn init() {
    let inc = (1. / sample_count as f64).exp2();
    let mut y = (1 << 30) as f64;
    for i in 0..sample_count {
        exp2_table[((i << 1) + 1) as usize] = MaybeUninit::new((y + 0.5).floor() as _);
        y *= inc;
    }
    for i in 0..sample_count - 1 {
        let value = exp2_table[((i << 1) + 3) as usize].assume_init() - exp2_table[((i << 1) + 1) as usize].assume_init();
        exp2_table[(i << 1) as usize] = MaybeUninit::new(value);
    }
    let value = (1u32 << 31u32) - exp2_table[((sample_count << 1) - 1) as usize].assume_init() as u32;
    exp2_table[((sample_count << 1) - 2) as usize] = MaybeUninit::new(value as _);
}

pub fn lookup(initialized: crate::initialize::Initialized, x: i32) -> i32 {
    const shift: i32 = 24 - lg_sample_count;
    let low_bits = x & ((1 << shift) - 1);
    let x_int = (x >> (shift - 1)) & ((sample_count - 1) << 1);
    let dy = unsafe { exp2_table[x_int as usize].assume_init() };
    let y0 = unsafe { exp2_table[(x_int + 1) as usize].assume_init() };
    let y = (y0 as i64 + ((dy as i64 * low_bits as i64) >> shift)) as i32;
    y >> (6 - (x >> 24))
}
