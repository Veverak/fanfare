const lg_buffer_size: i32 = 6;
const buffer_size: i32 = 1 << lg_buffer_size;

pub struct FmCore {
    buf1: crate::Buffer,
    buf2: crate::Buffer,
}

pub const algorithms: [[u8; 6]; 32] = [
    [0xc1, 0x11, 0x11, 0x14, 0x01, 0x14],
    [0x01, 0x11, 0x11, 0x14, 0xc1, 0x14],
    [0xc1, 0x11, 0x14, 0x01, 0x11, 0x14],
    [0xc1, 0x11, 0x94, 0x01, 0x11, 0x14],
    [0xc1, 0x14, 0x01, 0x14, 0x01, 0x14],
    [0xc1, 0x94, 0x01, 0x14, 0x01, 0x14],
    [0xc1, 0x11, 0x05, 0x14, 0x01, 0x14],
    [0x01, 0x11, 0xc5, 0x14, 0x01, 0x14],
    [0x01, 0x11, 0x05, 0x14, 0xc1, 0x14],
    [0x01, 0x05, 0x14, 0xc1, 0x11, 0x14],
    [0xc1, 0x05, 0x14, 0x01, 0x11, 0x14],
    [0x01, 0x05, 0x05, 0x14, 0xc1, 0x14],
    [0xc1, 0x05, 0x05, 0x14, 0x01, 0x14],
    [0xc1, 0x05, 0x11, 0x14, 0x01, 0x14],
    [0x01, 0x05, 0x11, 0x14, 0xc1, 0x14],
    [0xc1, 0x11, 0x02, 0x25, 0x05, 0x14],
    [0x01, 0x11, 0x02, 0x25, 0xc5, 0x14],
    [0x01, 0x11, 0x11, 0xc5, 0x05, 0x14],
    [0xc1, 0x14, 0x14, 0x01, 0x11, 0x14],
    [0x01, 0x05, 0x14, 0xc1, 0x14, 0x14],
    [0x01, 0x14, 0x14, 0xc1, 0x14, 0x14],
    [0xc1, 0x14, 0x14, 0x14, 0x01, 0x14],
    [0xc1, 0x14, 0x14, 0x01, 0x14, 0x04],
    [0xc1, 0x14, 0x14, 0x14, 0x04, 0x04],
    [0xc1, 0x14, 0x14, 0x04, 0x04, 0x04],
    [0xc1, 0x05, 0x14, 0x01, 0x14, 0x04],
    [0x01, 0x05, 0x14, 0xc1, 0x14, 0x04],
    [0x04, 0xc1, 0x11, 0x14, 0x01, 0x14],
    [0xc1, 0x14, 0x01, 0x14, 0x04, 0x04],
    [0x04, 0xc1, 0x11, 0x14, 0x04, 0x04],
    [0xc1, 0x14, 0x04, 0x04, 0x04, 0x04],
    [0xc4, 0x04, 0x04, 0x04, 0x04, 0x04],
];

pub const compatibility_with_specialized: bool = cfg!(feature = "compatibility_with_specialized");

const out_bus_one: u8 = 1 << 0;
const out_bus_two: u8 = 1 << 1;
const out_bus_add: u8 = 1 << 2;
const in_bus_one: u8 = 1 << 4;
const in_bus_two: u8 = 1 << 5;
const fb_in: u8 = 1 << 6;
const fb_out: u8 = 1 << 7;

fn n_out(algorithm: [u8; 6]) -> i32 {
    algorithm.iter().filter(|operator| *operator & 7 == out_bus_add).count() as _
}

pub fn dump() {
    for (algorithm_index, algorithm) in algorithms.iter().enumerate() {
        eprint!("{}:", algorithm_index + 1);
        for flags in algorithm {
            eprint!(" ");
            if flags & fb_in > 0 {
                eprint!("[");
            }
            let in_bus = if flags & in_bus_one > 0 {
                '1'
            } else if flags & in_bus_two > 0 {
                '2'
            } else {
                '0'
            };
            let out_bus = if flags & out_bus_one > 0 {
                '1'
            } else if flags & out_bus_two > 0 {
                '2'
            } else {
                '0'
            };
            eprint!("{}->{}", in_bus, out_bus);
            if flags & out_bus_add > 0 {
                eprint!("+");
            }
            if flags & fb_out > 0 {
                eprint!("]");
            }
        }
        eprintln!(" {}", n_out(*algorithm));
    }
}

impl FmCore {
    pub fn render(
        &mut self,
        initialized: crate::initialize::Initialized,
        output: &mut crate::Buffer,
        params: &mut [crate::fm_op_kernel::FmOpParams; 6],
        algorithm: [u8; 6],
        feedback_buffer: &mut [i32; 2],
        feedback_shift: i32,
    ) {
        let k_level_thresh = 1120;
        let mut has_contents = [true, false, false];
        for (operator_index, &flags) in algorithm.iter().enumerate() {
            let mut add = flags & out_bus_add > 0;
            let param = &mut params[operator_index];
            let inbus = (flags >> 4) & 3;
            let outbus = flags & 3;
            let (inptr, outptr) = match (inbus, outbus) {
                (0, 0) => (&[][..], &mut *output),
                (1, 0) => (&self.buf1[..], &mut *output),
                (2, 0) => (&self.buf2[..], &mut *output),
                (0, 1) | (1, 1) => (&[][..], &mut self.buf1),
                (2, 1) => (&self.buf2[..], &mut self.buf1),
                (0, 2) => (&[][..], &mut self.buf2),
                (1, 2) | (2, 2) => (&self.buf1[..], &mut self.buf2),
                _ => {
                    panic!();
                }
            };
            let gain1 = param.gain_out;
            let gain2 = crate::exp2::lookup(initialized, param.level_in - 14 * (1 << 24));
            param.gain_out = gain2;
            if gain1 >= k_level_thresh || gain2 >= k_level_thresh || compatibility_with_specialized && operator_index & 1 == 0 {
                if !has_contents[outbus as usize] {
                    add = false;
                }
                if inbus > 0 && has_contents[inbus as usize] {
                    if inbus == outbus {
                        crate::fm_op_kernel::compute_in_situ(initialized, outptr, param.phase, param.frequency, gain1, gain2, add);
                    } else {
                        crate::fm_op_kernel::compute(initialized, outptr, inptr, param.phase, param.frequency, gain1, gain2, add);
                    }
                } else if flags & 0xc0 == 0xc0 && feedback_shift < 16 {
                    crate::fm_op_kernel::compute_feedback(initialized, outptr, param.phase, param.frequency, gain1, gain2, feedback_buffer, feedback_shift, add);
                } else {
                    crate::fm_op_kernel::compute_pure(initialized, outptr, param.phase, param.frequency, gain1, gain2, add);
                }
                has_contents[outbus as usize] = true;
            } else if !add {
                has_contents[outbus as usize] = false;
            }
            param.phase = param.phase.wrapping_add(param.frequency << lg_buffer_size);
        }
    }
}

impl Default for FmCore {
    fn default() -> Self {
        FmCore {
            buf1: [0; buffer_size as usize],
            buf2: [0; buffer_size as usize],
        }
    }
}
