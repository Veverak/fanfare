#[derive(Clone, Copy, Default)]
pub struct Envelope {
    rates: [u8; 4],
    levels: [u8; 4],
    out_level: i32,
    rate_scaling: i32,
    level: i32,
    target_level: i32,
    rising: bool,
    ix: u8,
    increment: i32,
    #[cfg(feature = "dexed")]
    static_count: i32,
    down: bool,
}

const accurate_envelope: bool = crate::dexed;

const level_table: [i32; 20] = [0, 5, 9, 13, 17, 20, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 42, 43, 45, 46];

const statics: [i32; 77] = [
    1764000, 1764000, 1411200, 1411200, 1190700, 1014300, 992250,
    882000, 705600, 705600, 584325, 507150, 502740, 441000, 418950,
    352800, 308700, 286650, 253575, 220500, 220500, 176400, 145530,
    145530, 125685, 110250, 110250, 88200, 88200, 74970, 61740,
    61740, 55125, 48510, 44100, 37485, 31311, 30870, 27562, 27562,
    22050, 18522, 17640, 15435, 14112, 13230, 11025, 9261, 9261, 7717,
    6615, 6615, 5512, 5512, 4410, 3969, 3969, 3439, 2866, 2690, 2249,
    1984, 1896, 1808, 1411, 1367, 1234, 1146, 926, 837, 837, 705,
    573, 573, 529, 441, 441,
];

static mut sr_multiplier: u32 = 1 << 24;

impl Envelope {
    #[cfg(any(feature = "dexed", feature = "music_synthesizer_for_android", feature = "webdx7"))]
    pub fn dump(&self) -> crate::legacy::EnvelopeDump {
        crate::legacy::EnvelopeDump {
            rates: [self.rates[0] as _, self.rates[1] as _, self.rates[2] as _, self.rates[3] as _],
            levels: [self.levels[0] as _, self.levels[1] as _, self.levels[2] as _, self.levels[3] as _],
            out_level: self.out_level,
            rate_scaling: self.rate_scaling,
            level: self.level,
            target_level: self.target_level,
            rising: self.rising,
            ix: self.ix as _,
            increment: self.increment,
            #[cfg(feature = "dexed")]
            static_count: self.static_count,
            #[cfg(not(feature = "dexed"))]
            static_count: 0,
            down: self.down,
        }
    }

    pub fn init(&mut self, rates: [u8; 4], levels: [u8; 4], out_level: i32, rate_scaling: i32) {
        self.rates = rates;
        self.levels = levels;
        self.out_level = out_level;
        self.rate_scaling = rate_scaling;
        self.level = 0;
        self.down = true;
        self.advance(0);
    }

    pub fn update(&mut self, rates: [u8; 4], levels: [u8; 4], out_level: i32, rate_scaling: i32) {
        self.rates = rates;
        self.levels = levels;
        self.out_level = out_level;
        self.rate_scaling = rate_scaling;
        if self.down {
            let new_level = self.levels[2];
            let actual_level = scale_out_level(new_level as _) >> 1;
            let actual_level = (actual_level << 6) + self.out_level - 4256;
            let actual_level = actual_level.max(16);
            self.target_level = actual_level << 16;
            self.advance(2);
        }
    }

    pub fn get_sample(&mut self) -> i32 {
        #[cfg(feature = "dexed")]
        if accurate_envelope && self.static_count != 0 {
            self.static_count -= crate::buffer_size as i32;
            if self.static_count <= 0 {
                self.static_count = 0;
                self.advance(self.ix + 1);
            }
        }
        #[cfg(feature = "dexed")]
        let condition = self.static_count == 0;
        #[cfg(not(feature = "dexed"))]
        let condition = true;
        if (self.ix < 3 || self.ix < 4 && !self.down) && condition {
            if self.rising {
                let jump_target = 1716 << 16;
                if self.level < jump_target {
                    self.level = jump_target;
                }
                self.level += (((17 << 24) - self.level) >> 24) * self.increment;
                if self.level >= self.target_level {
                    self.level = self.target_level;
                    self.advance(self.ix + 1);
                }
            } else {
                self.level -= self.increment;
                if self.level <= self.target_level {
                    self.level = self.target_level;
                    self.advance(self.ix + 1);
                }
            }
        }
        self.level
    }

    pub fn key_down(&mut self, down: bool) {
        if self.down != down {
            self.down = down;
            self.advance(if down { 0 } else { 3 });
        }
    }

    pub unsafe fn init_sr(sample_rate: f64) {
        sr_multiplier = ((44100. / sample_rate) * (1 << 24) as f64) as _;
    }

    fn advance(&mut self, ix: u8) {
        self.ix = ix;
        if ix < 4 {
            let new_level = self.levels[ix as usize];
            let actual_level = scale_out_level(new_level as _) >> 1;
            let actual_level = (actual_level << 6) + self.out_level - 4256;
            let actual_level = actual_level.max(16);
            self.target_level = actual_level << 16;
            self.rising = self.target_level > self.level;
            let qrate = (41 * self.rates[ix as usize] as i32) >> 6;
            let qrate = qrate + self.rate_scaling;
            let qrate = qrate.min(63);
            #[cfg(feature = "dexed")]
            if accurate_envelope {
                if self.target_level == self.level || ix == 0 && new_level == 0 {
                    let static_rate = (self.rates[ix as usize] as i32 + self.rate_scaling).min(99);
                    self.static_count = match statics.get(static_rate as usize) {
                        None => 20 * (99 - static_rate),
                        Some(value) => *value,
                    };
                    if static_rate < 77 && (ix == 0 && new_level == 0) {
                        self.static_count /= 20;
                    }
                    self.static_count = ((self.static_count as i64 * unsafe { sr_multiplier } as i64) >> 24) as _;
                } else {
                    self.static_count = 0;
                }
            }
            self.increment = (4 + (qrate & 3)) << (2 + crate::lg_buffer_size + (qrate >> 2));
            self.increment = ((self.increment as i64 * unsafe { sr_multiplier } as i64) >> 24) as _;
        }
    }
}

pub fn scale_out_level(out_level: i32) -> i32 {
    match level_table.get(out_level as usize) {
        None => 28 + out_level,
        Some(value) => *value,
    }
}
