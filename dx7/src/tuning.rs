use std::mem::MaybeUninit;

static mut table: [MaybeUninit<i32>; 128] = [MaybeUninit::uninit(); 128];

pub unsafe fn init() {
    const base: i32 = 50857777;
    const step: i32 = (1 << 24) / 12;
    for (midi_note, entry) in table.iter_mut().enumerate() {
        *entry = MaybeUninit::new(base + step * midi_note as i32);
    }
}

pub fn midi_note_to_log_frequency(initialized: crate::initialize::Initialized, midi_note: i32) -> i32 {
    unsafe { table[midi_note as usize].assume_init() }
}
