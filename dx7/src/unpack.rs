use std::convert::TryInto;

pub fn unpack_patch(patch_source: &[u8; 128]) -> Option<[u8; 156]> {
    let mut patch_target = [0; 156];
    for oscillator_index in 0..6 {
        const oscillator_source_size: usize = 17;
        let oscillator_source_offset = oscillator_source_size * oscillator_index;
        let oscillator_source: &[u8; oscillator_source_size] = (&patch_source[oscillator_source_offset..oscillator_source_offset + oscillator_source_size])
            .try_into()
            .unwrap();
        const oscillator_target_size: usize = 21;
        let oscillator_target_offset = oscillator_target_size * oscillator_index;
        let oscillator_target: &mut [u8; oscillator_target_size] = (&mut patch_target[oscillator_target_offset..oscillator_target_offset + oscillator_target_size])
            .try_into()
            .unwrap();
        for i in 0..11 {
            oscillator_target[i] = cap(oscillator_source[i] & 0x7f, 99)?;
        }
        let left_right_curves = oscillator_source[11] & 0xf;
        oscillator_target[11] = left_right_curves & 3;
        oscillator_target[12] = (left_right_curves >> 2) & 3;
        let detune_rs = oscillator_source[12] & 0x7f;
        oscillator_target[13] = detune_rs & 7;
        let kvs_ams = oscillator_source[13] & 0x1f;
        oscillator_target[14] = kvs_ams & 3;
        oscillator_target[15] = (kvs_ams >> 2) & 7;
        oscillator_target[16] = oscillator_source[14] & 0x7f; 
        let fcoarse_mode = oscillator_source[15] & 0x3f;
        oscillator_target[17] = fcoarse_mode & 1;
        oscillator_target[18] = (fcoarse_mode >> 1) & 0x1f;
        oscillator_target[19] = oscillator_source[16] & 0x7f; 
        oscillator_target[20] = (detune_rs >> 3) & 0x7f;
    }
    for i in 0..8 {
        patch_target[i + 126] = cap(patch_source[i + 102] & 0x7f, 99)?;
    }
    patch_target[134] = cap(patch_source[110] & 0x1f, 31)?;
    let oks_fb = patch_source[111] & 0xf;
    patch_target[135] = oks_fb & 7;
    patch_target[136] = oks_fb >> 3;
    patch_target[137] = patch_source[112] & 0x7f;
    patch_target[138] = patch_source[113] & 0x7f;
    patch_target[139] = patch_source[114] & 0x7f;
    patch_target[140] = patch_source[115] & 0x7f;
    let lpms_lfw_lks = patch_source[116] & 0x7f;
    patch_target[141] = lpms_lfw_lks & 1;
    patch_target[142] = (lpms_lfw_lks >> 1) & 7;
    patch_target[143] = lpms_lfw_lks >> 4;
    patch_target[144] = patch_source[117] & 0x7f;
    for offset in 0..10 {
        patch_target[offset + 145] = patch_source[offset + 118] & 0x7f;
    }
    patch_target[155] = 0x3f;
    Some(patch_target)
}

fn cap(source: u8, max: u8) -> Option<u8> {
    if source > max {
        None
    } else {
        Some(source)
    }
}
