use std::mem::MaybeUninit;

const lg_sample_count: i32 = 10;
const sample_count: i32 = 1 << lg_sample_count;

static mut tanh_table: [MaybeUninit<i32>; (sample_count << 1) as usize] = [MaybeUninit::uninit(); (sample_count << 1) as usize];

fn dtanh(y: f64) -> f64 {
    1. - y * y
}

pub unsafe fn init() {
    let step = 4. / sample_count as f64;
    let mut y = 0.;
    for i in 0..sample_count {
        tanh_table[((i << 1) + 1) as usize] = MaybeUninit::new(((1 << 24) as f64 * y + 0.5) as _);
        let k1 = dtanh(y);
        let k2 = dtanh(y + 0.5 * step * k1);
        let k3 = dtanh(y + 0.5 * step * k2);
        let k4 = dtanh(y + step * k3);
        let dy = (step / 6.) * (k1 + k4 + 2. * (k2 + k3));
        y += dy;
    }
    for i in 0..sample_count - 1 {
        let value = tanh_table[((i << 1) + 3) as usize].assume_init() - tanh_table[((i << 1) + 1) as usize].assume_init();
        tanh_table[(i << 1) as usize] = MaybeUninit::new(value);
    }
    let last_y = ((1 << 24) as f64 * y + 0.5) as i32;
    let value = last_y - tanh_table[((sample_count << 1) - 1) as usize].assume_init();
    tanh_table[((sample_count << 1) - 2) as usize] = MaybeUninit::new(value);
}

pub fn lookup(initialized: crate::initialize::Initialized, x: i32) -> i32 {
    let signum = x >> 31;
    let x = x ^ signum;
    if x < (4 << 24) {
        const shift: i32 = 26 - lg_sample_count;
        let low_bits = x & ((1 << shift) - 1);
        let x_int = (x >> (shift - 1)) & ((sample_count - 1) << 1);
        let dy = unsafe { tanh_table[(x_int) as usize].assume_init() };
        let y0 = unsafe { tanh_table[(x_int + 1) as usize].assume_init() };
        let y = (y0 as i64 + ((dy as i64 * low_bits as i64) >> shift)) as i32;
        y ^ signum
    } else if x < (17 << 23) {
        let sx = ((-48408812 * x as i64) >> 24) as i32;
        signum ^ ((1 << 24) - 2 * crate::exp2::lookup(initialized, sx))
    } else {
        signum ^ (1 << 24)
    }
}
