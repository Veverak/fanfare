#[derive(Default)]
pub struct PitchEnvelope {
    rates: [i32; 4],
    levels: [i32; 4],
    level: i32,
    target_level: i32,
    rising: bool,
    ix: i32,
    increment: i32,
    down: bool,
}

const lg_n: i32 = 6;
const n: i32 = 1 << lg_n;

const pitch_envelope_rate: [u8; 100] = [
    1, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12,
    12, 13, 13, 14, 14, 15, 16, 16, 17, 18, 18, 19, 20, 21, 22, 23, 24,
    25, 26, 27, 28, 30, 31, 33, 34, 36, 37, 38, 39, 41, 42, 44, 46, 47,
    49, 51, 53, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 79, 82,
    85, 88, 91, 94, 98, 102, 106, 110, 115, 120, 125, 130, 135, 141, 147,
    153, 159, 165, 171, 178, 185, 193, 202, 211, 232, 243, 254, 255,
];

const pitch_envelope_table: [i8; 100] = [
    -128, -116, -104, -95, -85, -76, -68, -61, -56, -52, -49, -46, -43,
    -41, -39, -37, -35, -33, -32, -31, -30, -29, -28, -27, -26, -25, -24,
    -23, -22, -21, -20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10,
    -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
    28, 29, 30, 31, 32, 33, 34, 35, 38, 40, 43, 46, 49, 53, 58, 65, 73,
    82, 92, 103, 115, 127,
];

static mut unit: i32 = 0;

impl PitchEnvelope {
    #[cfg(any(feature = "dexed", feature = "music_synthesizer_for_android", feature = "webdx7"))]
    pub fn dump(&self) -> crate::legacy::PitchEnvelopeDump {
        crate::legacy::PitchEnvelopeDump {
            rates: self.rates,
            levels: self.levels,
            level: self.level,
            target_level: self.target_level,
            rising: self.rising,
            ix: self.ix,
            increment: self.increment,
            down: self.down,
        }
    }

    pub fn set(&mut self, initialized: crate::initialize::Initialized, rates: [i32; 4], levels: [i32; 4]) {
        self.rates = rates;
        self.levels = levels;
        self.level = (pitch_envelope_table[levels[3] as usize] as i32) << 19;
        self.down = true;
        self.advance(initialized, 0);
    }

    pub fn get_sample(&mut self, initialized: crate::initialize::Initialized) -> i32 {
        if self.ix < 3 || self.ix < 4 && !self.down {
            if self.rising {
                self.level += self.increment;
                if self.level >= self.target_level {
                    self.level = self.target_level;
                    self.advance(initialized, self.ix + 1);
                }
            } else {
                self.level -= self.increment;
                if self.level <= self.target_level {
                    self.level = self.target_level;
                    self.advance(initialized, self.ix + 1);
                }
            }
        }
        self.level
    }

    pub fn key_down(&mut self, initialized: crate::initialize::Initialized, down: bool) {
        if self.down != down {
            self.down = down;
            self.advance(initialized, if down { 0 } else { 3 });
        }
    }

    fn advance(&mut self, initialized: crate::initialize::Initialized, ix: i32) {
        self.ix = ix;
        if ix < 4 {
            let new_level = self.levels[ix as usize];
            self.target_level = (pitch_envelope_table[new_level as usize] as i32) << 19;
            self.rising = self.target_level > self.level;
            self.increment = (pitch_envelope_rate[self.rates[ix as usize] as usize] as i32) * unsafe { unit };
        }
    }
}

pub unsafe fn init(sample_rate: f64) {
    unit = ((n * (1 << 24)) as f64 / (21.3 * sample_rate) + 0.5) as _;
}
