#!/bin/sh
git clone -n https://github.com/google/music-synthesizer-for-android.git music_synthesizer_for_android
cd music_synthesizer_for_android
git checkout f67d41d313b7dc85f6fb99e79e515cc9d208cfff
git apply ../music_synthesizer_for_android.patch
