#define private public

#include "$include$/synth.h"
#include "$include$/controllers.h"
#include "$include$/dx7note.h"
#ifndef DEXED
#include "$include$/fm_core.h"
#endif
#include "$include$/lfo.h"
#ifndef DEXED
#include "$include$/patch.h"
#endif
#include "$include$/exp2.h"
#include "$include$/freqlut.h"
#include "$include$/sin.h"
#ifdef DEXED
#include "$include$/tuning.h"
#endif

extern "C" {
    typedef struct EnvelopeDump {
        int32_t rates[4];
        int32_t levels[4];
        int32_t out_level;
        int32_t rate_scaling;
        int32_t level;
        int32_t target_level;
        bool rising;
        int32_t ix;
        int32_t increment;
        int32_t static_count;
        bool down;
    } envelope_dump_t;

    typedef struct LfoDump {
        uint32_t phase;
        uint32_t delta;
        uint8_t waveform;
        uint8_t random_state;
        bool sync;
        uint32_t delay_state;
        uint32_t delay_increment1;
        uint32_t delay_increment2;
    } lfo_dump_t;

    typedef struct PitchEnvelopeDump {
        int32_t rates[4];
        int32_t levels[4];
        int32_t level;
        int32_t target_level;
        bool rising;
        int32_t ix;
        int32_t increment;
        bool down;
    } pitch_envelope_dump_t;

    typedef struct NoteDump {
        EnvelopeDump envelope[6];
        PitchEnvelopeDump pitch_envelope;
        int32_t base_pitch[6];
        int32_t feedback_buffer[2];
        int32_t feedback_shift;
        int32_t pitch_mod_depth;
        int32_t pitch_mod_sens;
    } note_dump_t;

    static envelope_dump_t envelope_dump(Env *envelope) {
        return EnvelopeDump {
            rates: {
                envelope->rates_[0],
                envelope->rates_[1],
                envelope->rates_[2],
                envelope->rates_[3],
            },
            levels: {
                envelope->levels_[0],
                envelope->levels_[1],
                envelope->levels_[2],
                envelope->levels_[3],
            },
            out_level: envelope->outlevel_,
            rate_scaling: envelope->rate_scaling_,
            level: envelope->level_,
            target_level: envelope->targetlevel_,
            rising: envelope->rising_,
            ix: envelope->ix_,
            increment: envelope->inc_,
            #ifdef DEXED
            static_count: envelope->staticcount_,
            #else
            static_count: 0,
            #endif
            down: envelope->down_,
        };
    }

    static pitch_envelope_dump_t pitch_envelope_dump(PitchEnv *envelope) {
        return PitchEnvelopeDump {
            rates: {
                envelope->rates_[0],
                envelope->rates_[1],
                envelope->rates_[2],
                envelope->rates_[3],
            },
            levels: {
                envelope->levels_[0],
                envelope->levels_[1],
                envelope->levels_[2],
                envelope->levels_[3],
            },
            level: envelope->level_,
            target_level: envelope->targetlevel_,
            rising: envelope->rising_,
            ix: envelope->ix_,
            increment: envelope->inc_,
            down: envelope->down_,
        };
    }

    void *controllers_create() {
        auto controllers = new Controllers();
        controllers->values_[kControllerPitch] = 0x2000;
        #ifdef DEXED
        controllers->core = new FmCore();
        controllers->refresh();
        #endif
        return controllers;
    }

    void init() {
        Exp2::init();
        Freqlut::init(44100);
        Lfo::init(44100);
        PitchEnv::init(44100);
        Tanh::init();
        Sin::init();
    }

    int32_t exp2_lookup(int32_t x) {
        return Exp2::lookup(x);
    }

    int32_t frequency_lookup(int32_t log_frequency) {
        return Freqlut::lookup(log_frequency);
    }

    void *lfo_create() {
        return new Lfo();
    }

    lfo_dump_t lfo_dump(Lfo *lfo) {
        return LfoDump {
            phase: lfo->phase_,
            delta: lfo->delta_,
            waveform: lfo->waveform_,
            random_state: lfo->randstate_,
            sync: lfo->sync_,
            delay_state: lfo->delaystate_,
            delay_increment1: lfo->delayinc_,
            delay_increment2: lfo->delayinc2_,
        };
    }

    int32_t lfo_get_delay(Lfo *lfo) {
        return lfo->getdelay();
    }

    int32_t lfo_get_sample(Lfo *lfo) {
        return lfo->getsample();
    }

    void lfo_key_down(Lfo *lfo) {
        lfo->keydown();
    }

    void lfo_reset(Lfo *lfo, const char params[6]) {
        lfo->reset(params);
    }

    void note_compute(Dx7Note *note, int32_t *output, int32_t lfo_val, int32_t lfo_delay, Controllers *controllers) {
        note->compute(output, lfo_val, lfo_delay, controllers);
    }

    void *note_create() {
        #ifdef DEXED
        return new Dx7Note(createStandardTuning());
        #else
        return new Dx7Note();
        #endif
    }

    note_dump_t note_dump(Dx7Note *note) {
        return NoteDump {
            envelope: {
                envelope_dump(note->env_),
                envelope_dump(note->env_ + 1),
                envelope_dump(note->env_ + 2),
                envelope_dump(note->env_ + 3),
                envelope_dump(note->env_ + 4),
                envelope_dump(note->env_ + 5),
            },
            pitch_envelope: pitch_envelope_dump(&note->pitchenv_),
            base_pitch: { note->basepitch_[0], note->basepitch_[1], note->basepitch_[2], note->basepitch_[3], note->basepitch_[4], note->basepitch_[5] },
            feedback_buffer: { note->fb_buf_[0], note->fb_buf_[1] },
            feedback_shift: note->fb_shift_,
            pitch_mod_depth: note->pitchmoddepth_,
            pitch_mod_sens: note->pitchmodsens_,
        };
    }

    void note_init(Dx7Note *note, const char patch[128], int midi_note, int velocity) {
        note->init(patch, midi_note, velocity);
    }

    void note_key_up(Dx7Note *note) {
        note->keyup();
    }

    int32_t sin_lookup(int32_t phase) {
        return Sin::lookup(phase);
    }

    int32_t tanh_lookup(int32_t x) {
        return Tanh::lookup(x);
    }

    #ifndef DEXED
    void unpack_patch(const char source[128], char target[156]) {
        UnpackPatch(source, target);
    }
    #endif
}
