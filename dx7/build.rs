#![forbid(non_ascii_idents)]

fn main() {
    if cfg!(feature = "dexed") {
        build_legacy("dexed", "Source/msfa", true);
    }
    if cfg!(feature = "music_synthesizer_for_android") {
        build_legacy("music_synthesizer_for_android", "app/src/main/jni", false);
    }
    if cfg!(feature = "webdx7") {
        build_legacy("webdx7", "src/c/msfa", false);
    }
}

fn build_legacy(name: &str, source_path: &str, dexed: bool) {
    let source_path = format!("../../legacy/{}/{}", name, source_path);
    let source = std::fs::read_to_string("legacy/main.cpp").unwrap();
    let source = source.replace("$include$", &source_path);
    let target_path = format!("legacy_target/{}", name);
    std::fs::create_dir_all(&target_path).unwrap();
    std::fs::write(format!("{}/main.cpp", target_path), source).unwrap();
    let module = if dexed {
        "tuning"
    } else {
        "patch"
    };
    let modules = ["dx7note", "env", "exp2", "fm_core", "fm_op_kernel", "freqlut", "lfo", "pitchenv", "sin", module];
    let mut command = std::process::Command::new("g++");
    command.args(&[
        "-c",
        "-fPIC",
        "-std=c++11",
        "main.cpp",
    ]);
    for module in &modules {
        command.arg(format!("{}/{}.cc", source_path, module));
    }
    if dexed {
        command.arg("-DDEXED");
        command.arg("-fpermissive");
    }
    command.current_dir(&target_path);
    assert!(command.status().unwrap().success());
    let mut command = std::process::Command::new("ar");
    command.args(&["-crs", "libsynth.a", "main.o"]);
    for module in &modules {
        command.arg(format!("{}.o", module));
    }
    command.current_dir(&target_path);
    assert!(command.status().unwrap().success());
    println!("cargo:rustc-flags=-l stdc++");
    println!("cargo:rustc-link-search=native=legacy_target/{}", name);
}
