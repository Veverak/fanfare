#!/bin/sh
set -e
wget https://www.webaudiomodules.org/patches/webdx7/dx7patches.zip
echo "a49b0df6e7f63b1d59cbf3f1ebdd467b769c89c9a7a8e6fa7e0b6274a4bb901f dx7patches.zip" | sha256sum -c
unzip dx7patches.zip
rm dx7patches.zip
echo "ca9b885037e174860cd3244ff862dc1b81ce7be46a262e67c836ccd2a052ddf2 dx7patches.lib" | sha256sum -c
