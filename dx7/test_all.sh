#!/bin/sh
set -e
cargo run --features dexed test patches/rom1A.syx
cargo run --features music_synthesizer_for_android test patches/rom1A.syx
cargo run --features webdx7 test patches/rom1A.syx
cargo run --features compatibility_with_specialized verify_specialized patches/dx7patches.lib
