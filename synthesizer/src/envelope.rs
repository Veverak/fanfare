#[derive(Clone, Copy, Default)]
pub struct Envelope {
    increment: i32,
    ix: u8,
    level: i32,
    levels: [u8; 4],
    out_level: i32,
    rate_scaling: i32,
    rates: [u8; 4],
    rising: bool,
    target_level: i32,
}

impl Envelope {
    pub fn init(&mut self, rates: [u8; 4], levels: [u8; 4], out_level: i32, rate_scaling: i32) {
        self.level = 0;
        self.levels = levels;
        self.out_level = out_level;
        self.rate_scaling = rate_scaling;
        self.rates = rates;
        self.advance(0);
    }

    pub fn get_sample(&mut self, down: bool) -> i32 {
        if self.ix < 3 || self.ix < 4 && !down {
            if self.rising {
                let jump_target = 1716 << 16;
                if self.level < jump_target {
                    self.level = jump_target;
                }
                self.level += (((17 << 24) - self.level) >> 24) * self.increment;
                if self.level >= self.target_level {
                    self.level = self.target_level;
                    self.advance(self.ix + 1);
                }
            } else {
                self.level -= self.increment;
                if self.level <= self.target_level {
                    self.level = self.target_level;
                    self.advance(self.ix + 1);
                }
            }
        }
        self.level
    }

    pub fn key_up(&mut self) {
        self.advance(3);
    }

    fn advance(&mut self, ix: u8) {
        self.ix = ix;
        if ix < 4 {
            let actual_level = self.levels[ix as usize] as i32;
            let actual_level = (actual_level << 6) + self.out_level - 4256;
            let actual_level = actual_level.max(16);
            self.target_level = actual_level << 16;
            self.rising = self.target_level > self.level;
            let qrate = self.rates[ix as usize] as i32;
            let qrate = qrate + self.rate_scaling;
            let qrate = qrate.min(63);
            self.increment = (4 + (qrate & 3)) << (2 + crate::lg_buffer_size + (qrate >> 2));
        }
    }
}
