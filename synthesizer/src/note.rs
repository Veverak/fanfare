#[derive(Default)]
pub struct Note {
    down: bool,
    feedback_buffer: [i32; 2],
    oscillators: [OscillatorState; 6],
}

#[derive(Default)]
struct OscillatorState {
    envelope: crate::envelope::Envelope,
    frequency: i32,
    gain_out: i32,
    phase: i32,
}

fn scale_rate(midi_note: i32) -> i32 {
    let x = (midi_note / 3 - 7).max(0).min(31);
    (crate::configuration::rate_scaling_sensitivity * x) >> 3
}

impl Note {
    pub fn initialize(&mut self, initialized: crate::Initialized, midi_note: u8) {
        self.down = true;
        let note_frequency = crate::tuning::frequency_from_midi_note(initialized, midi_note as _);
        let rate_scaling = scale_rate(midi_note as _);
        for (oscillator_index, oscillator) in crate::configuration::oscillators.iter().enumerate() {
            let oscillator_state = &mut self.oscillators[oscillator_index];
            oscillator_state.envelope.init(
                oscillator.rates,
                oscillator.levels,
                oscillator.out_level,
                oscillator.rate_scaling_mask as i32 & rate_scaling,
            );
            oscillator_state.frequency = ((note_frequency as i64 * oscillator.detune as i64) >> 24) as _;
        }
    }

    pub fn synthesize(
        &mut self,
        initialized: crate::Initialized,
        output_buffer: &mut crate::Buffer,
        internal_buffer: &mut crate::Buffer,
    ) {
        let k_level_thresh = 1120;
        for (oscillator_index, oscillator_state) in self.oscillators.iter_mut().enumerate() {
            let level_in = oscillator_state.envelope.get_sample(self.down);
            let gain1 = oscillator_state.gain_out;
            let gain2 = crate::exp2::lookup(initialized, level_in - 14 * (1 << 24));
            oscillator_state.gain_out = gain2;
            if gain1 >= k_level_thresh || gain2 >= k_level_thresh || oscillator_index & 1 == 0 {
                if oscillator_index == 0 {
                    crate::oscillator::compute_feedback(initialized, internal_buffer, oscillator_state.phase, oscillator_state.frequency, gain1, gain2, &mut self.feedback_buffer);
                } else if oscillator_index & 1 > 0 {
                    crate::oscillator::compute(initialized, output_buffer, internal_buffer, oscillator_state.phase, oscillator_state.frequency, gain1, gain2);
                } else {
                    crate::oscillator::compute_pure(initialized, internal_buffer, oscillator_state.phase, oscillator_state.frequency, gain1, gain2);
                }
            }
            oscillator_state.phase = oscillator_state.phase.wrapping_add(oscillator_state.frequency << crate::lg_buffer_size);
        }
    }

    pub fn key_up(&mut self) {
        if self.down {
            self.down = false;
            for oscillator_state in &mut self.oscillators {
                oscillator_state.envelope.key_up();
            }
        }
    }
}
