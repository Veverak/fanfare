use std::mem::MaybeUninit;

const lg_sample_count: i32 = 10;
const sample_count: i32 = 1 << lg_sample_count;

static mut table: [MaybeUninit<i32>; (sample_count as usize) << 1] = [MaybeUninit::uninit(); (sample_count as usize) << 1];

pub unsafe fn initialize() {
    const r: i64 = 1 << 29;
    // let phase = 2. * std::f64::consts::PI / sample_count as f64;
    // let c = (phase.cos() * (1 << 30) as f64 + 0.5).floor() as i32;
    let c = 1073721611;
    // let s = (phase.sin() * (1 << 30) as f64 + 0.5).floor() as i32;
    let s = 6588356;
    let mut u = 1 << 30;
    let mut v = 0;
    for i in 0..sample_count / 2 {
        table[((i << 1) + 1) as usize] = MaybeUninit::new((v + 32) >> 6);
        table[(((i + sample_count / 2) << 1) + 1) as usize] = MaybeUninit::new(-((v + 32) >> 6));
        let t = ((u as i64 * s as i64 + v as i64 * c as i64 + r) >> 30) as i32;
        u = ((u as i64 * c as i64 - v as i64 * s as i64 + r) >> 30) as i32;
        v = t;
    }
    for i in 0..sample_count - 1 {
        table[(i << 1) as usize] = MaybeUninit::new(table[((i << 1) + 3) as usize].assume_init() - table[((i << 1) + 1) as usize].assume_init());
    }
    table[((sample_count << 1) - 2) as usize] = MaybeUninit::new(-table[((sample_count << 1) - 1) as usize].assume_init());
}

pub fn lookup(_initialized: crate::Initialized, phase: i32) -> i32 {
    const shift: i32 = 24 - lg_sample_count;
    let low_bits = phase & ((1 << shift) - 1);
    let phase_int = (phase >> (shift - 1)) & ((sample_count - 1) << 1);
    let dy = unsafe { table[phase_int as usize].assume_init() };
    let y0 = unsafe { table[(phase_int + 1) as usize].assume_init() };
    (y0 as i64 + ((dy as i64 * low_bits as i64) >> shift)) as i32
}
