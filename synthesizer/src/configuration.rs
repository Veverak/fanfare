pub struct Oscillator {
    pub detune: i32,
    pub levels: [u8; 4],
    pub out_level: i32,
    pub rate_scaling_mask: i8,
    pub rates: [u8; 4],
}

pub const feedback: i32 = 6;

pub const feedback_bit_depth: i32 = 8;

pub const oscillators: [Oscillator; 6] = [
    Oscillator {
        detune: 16777216,
        levels: [63, 63, 0, 0],
        out_level: 3232,
        rate_scaling_mask: 0,
        rates: [63, 59, 18, 31],
    },
    Oscillator {
        detune: 16944988,
        levels: [63, 49, 0, 0],
        out_level: 4064,
        rate_scaling_mask: 0,
        rates: [63, 16, 14, 32],
    },
    Oscillator {
        detune: 16716163,
        levels: [63, 63, 0, 0],
        out_level: 3264,
        rate_scaling_mask: 0,
        rates: [63, 41, 22, 32],
    },
    Oscillator {
        detune: 16716163,
        levels: [63, 49, 0, 0],
        out_level: 4064,
        rate_scaling_mask: 0,
        rates: [63, 16, 14, 32],
    },
    Oscillator {
        detune: 234026278,
        levels: [63, 49, 0, 0],
        out_level: 3680,
        rate_scaling_mask: -1,
        rates: [62, 29, 22, 38],
    },
    Oscillator {
        detune: 16742301,
        levels: [63, 51, 0, 0],
        out_level: 4064,
        rate_scaling_mask: 0,
        rates: [63, 16, 14, 32],
    },
];

pub const rate_scaling_sensitivity: i32 = 3;
