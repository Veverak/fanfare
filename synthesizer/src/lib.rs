#![allow(non_upper_case_globals)]
#![forbid(non_ascii_idents)]

mod configuration;
mod envelope;
mod exp2;
mod initialize;
mod note;
mod oscillator;
mod sin;
mod tuning;

pub use initialize::{Initialized, initialize};
pub use note::Note;

const lg_buffer_size: i32 = 6;
pub const buffer_size: usize = (1 << lg_buffer_size) as usize;
pub const sample_rate: u32 = 44100;

type Buffer = [i32; buffer_size];
