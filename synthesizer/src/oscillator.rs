pub fn compute(
    initialized: crate::Initialized,
    output: &mut crate::Buffer,
    input: &[i32],
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    for sample_index in 0..crate::buffer_size {
        gain += dgain;
        let y = crate::sin::lookup(initialized, phase.wrapping_add(input[sample_index]));
        output[sample_index] += ((y as i64 * gain as i64) >> 24) as i32;
        phase = phase.wrapping_add(frequency);
    }
}

pub fn compute_feedback(
    initialized: crate::Initialized,
    output: &mut crate::Buffer,
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
    feedback_buffer: &mut [i32; 2],
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    let mut y0 = feedback_buffer[0];
    let mut y = feedback_buffer[1];
    for sample_index in 0..crate::buffer_size {
        gain += dgain;
        let scaled_feedback = (y0 + y) >> (crate::configuration::feedback_bit_depth - crate::configuration::feedback + 1);
        y0 = y;
        y = crate::sin::lookup(initialized, phase.wrapping_add(scaled_feedback));
        y = ((y as i64 * gain as i64) >> 24) as _;
        output[sample_index] = y;
        phase = phase.wrapping_add(frequency);
    }
    *feedback_buffer = [y0, y];
}

pub fn compute_pure(
    initialized: crate::Initialized,
    output: &mut crate::Buffer,
    phase0: i32,
    frequency: i32,
    gain1: i32,
    gain2: i32,
) {
    let dgain = (gain2 - gain1 + (crate::buffer_size as i32 >> 1)) >> crate::lg_buffer_size;
    let mut gain = gain1;
    let mut phase = phase0;
    for sample_index in 0..crate::buffer_size {
        gain += dgain;
        let y = crate::sin::lookup(initialized, phase);
        output[sample_index] = ((y as i64 * gain as i64) >> 24) as i32;
        phase = phase.wrapping_add(frequency);
    }
}
