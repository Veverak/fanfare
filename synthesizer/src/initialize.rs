#[derive(Clone, Copy)]
pub struct Initialized(());

pub unsafe fn initialize() -> Initialized {
    crate::exp2::initialize();
    crate::sin::initialize();
    crate::tuning::initialize();
    Initialized(())
}
