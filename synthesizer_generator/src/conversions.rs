pub const coarse_table: [i32; 32] = [
    -16777216, 0, 16777216, 26591258, 33554432, 38955489, 43368474, 47099600,
    50331648, 53182516, 55732705, 58039632, 60145690, 62083076, 63876816,
    65546747, 67108864, 68576247, 69959732, 71268397, 72509921, 73690858,
    74816848, 75892776, 76922906, 77910978, 78860292, 79773775, 80654032,
    81503396, 82323963, 83117622,
];

const level_table: [i32; 20] = [0, 5, 9, 13, 17, 20, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 42, 43, 45, 46];

pub fn frequency_from_log_frequency(log_frequency: i32) -> i32 {
    let multiplier = (1 << 24) as f64;
    (2f64.powf(log_frequency as f64 / multiplier) * multiplier).round() as _
}

pub fn scale_out_level(out_level: i32) -> i32 {
    match level_table.get(out_level as usize) {
        None => 28 + out_level,
        Some(value) => *value,
    }
}
