use std::{convert::TryInto, fmt::Write};

struct Oscillator {
    detune: i32,
    levels: [u8; 4],
    out_level: i32,
    rate_scaling_mask: i8,
    rates: [u8; 4],
}

pub fn generate(patch_source: [u8; 156]) -> String {
    let mut common_rate_scaling = None;
    let oscillators: Vec<_> = (0..6).map(|oscillator_index| {
        const oscillator_size: usize = 21;
        let oscillator_offset = oscillator_size * oscillator_index;
        let oscillator_source: &[u8; oscillator_size] = (&patch_source[oscillator_offset..oscillator_offset + oscillator_size]).try_into().unwrap();
        let convert_rate = |rate| ((41 * rate as i32) >> 6) as _;
        let rates = [
            convert_rate(oscillator_source[0]),
            convert_rate(oscillator_source[1]),
            convert_rate(oscillator_source[2]),
            convert_rate(oscillator_source[3]),
        ];
        let convert_level = |level| (crate::conversions::scale_out_level(level as _) >> 1).try_into().unwrap();
        let levels = [
            convert_level(oscillator_source[4]),
            convert_level(oscillator_source[5]),
            convert_level(oscillator_source[6]),
            convert_level(oscillator_source[7]),
        ];
        assert_eq!(oscillator_source[8], 0, "feature not supported (break point)");
        assert_eq!(oscillator_source[9], 0, "feature not supported (left depth)");
        assert_eq!(oscillator_source[10], 0, "feature not supported (right depth)");
        assert_eq!(oscillator_source[11], 0, "feature not supported (left curve)");
        assert_eq!(oscillator_source[12], 0, "feature not supported (right curve)");
        let rate_scaling = oscillator_source[13];
        assert_eq!(oscillator_source[14], 0, "feature not supported (amp mod sens)");
        let sensitivity = oscillator_source[15];
        let out_level = oscillator_source[16];
        assert_eq!(oscillator_source[17], 0, "feature not supported (mode)");
        if rate_scaling != 0 && common_rate_scaling != Some(rate_scaling) {
            if common_rate_scaling.is_some() {
                panic!("feature not supported (diverse rate scaling)");
            }
            common_rate_scaling = Some(rate_scaling);
        }
        let coarse = oscillator_source[18];
        let fine = oscillator_source[19];
        let detune = oscillator_source[20];
        let scaled_coarse = crate::conversions::coarse_table[coarse as usize];
        let scaled_detune = 12606 * (detune as i32 - 7);
        let scaled_fine = ((1 << 24) as f64 / std::f64::consts::LN_2 * ((fine + 100) as f64 / 100.).ln() + 0.5).floor() as i32;
        let scaled_sensitivity = ((15 * sensitivity as i32 + 7) >> 3) << 4;
        let scaled_out_level = crate::conversions::scale_out_level(out_level as _) << 5;
        assert!(scaled_out_level <= (127 << 5));
        assert!(scaled_out_level >= 0);
        Oscillator {
            detune: scaled_coarse + scaled_detune + scaled_fine,
            levels,
            out_level: scaled_sensitivity + scaled_out_level,
            rate_scaling_mask: if rate_scaling == 0 {
                0
            } else {
                -1
            },
            rates,
        }
    }).collect();
    let _pitch_envelope_rates = [patch_source[126], patch_source[127], patch_source[128], patch_source[129]];
    let pitch_envelope_levels = [patch_source[130], patch_source[131], patch_source[132], patch_source[133]];
    assert_eq!(pitch_envelope_levels, [50, 50, 50, 50], "feature not supported (pitch envelope)");
    let algorithm = patch_source[134];
    assert_eq!(algorithm, 4, "feature not supported (other algorithms than 5)");
    let feedback = patch_source[135];
    assert!(feedback > 0, "feature not supported (no feedback)");
    assert_eq!(patch_source[139], 0, "feature not supported (pitch mod depth)");
    assert_eq!(patch_source[140], 0, "feature not supported (amp mod depth)");
    let _pitch_mod_sens = patch_source[143];
    let mut output = String::new();
    macro_rules! append {
        ( $( $token:tt )+ ) => {
            writeln!(&mut output, $( $token )+).unwrap();
        }
    }
    append!(concat!(
        "pub struct Oscillator {{\n",
        "    pub detune: i32,\n",
        "    pub levels: [u8; 4],\n",
        "    pub out_level: i32,",
    ));
    if common_rate_scaling.is_some() {
        append!("    pub rate_scaling_mask: i8,");
    }
    append!(
        concat!(
            "    pub rates: [u8; 4],\n",
            "}}\n",
            "\n",
            "pub const feedback: i32 = {};\n",
            "\n",
            "pub const feedback_bit_depth: i32 = 8;\n",
            "\n",
            "pub const oscillators: [Oscillator; 6] = [",
        ),
        feedback,
    );
    for oscillator in oscillators {
        append!(
            concat!(
                "    Oscillator {{\n",
                "        detune: {},\n",
                "        levels: [{}, {}, {}, {}],\n",
                "        out_level: {},",
            ),
            crate::conversions::frequency_from_log_frequency(oscillator.detune),
            oscillator.levels[0],
            oscillator.levels[1],
            oscillator.levels[2],
            oscillator.levels[3],
            oscillator.out_level,
        );
        if common_rate_scaling.is_some() {
            append!("        rate_scaling_mask: {},", oscillator.rate_scaling_mask);
        }
        append!(
            concat!(
                "        rates: [{}, {}, {}, {}],\n",
                "    }},",
            ),
            oscillator.rates[0],
            oscillator.rates[1],
            oscillator.rates[2],
            oscillator.rates[3],
        );
    }
    append!("];\n");
    if let Some(rate_scaling_sensitivity) = common_rate_scaling {
        append!("pub const rate_scaling_sensitivity: i32 = {};", rate_scaling_sensitivity);
    }
    output
}
