use std::convert::TryInto;

const header: &[u8] = &[0xf0, 0x43, 0x00, 0x09, 0x20, 0x00];

pub fn load_patch(bank_file_path: &str, patch_index: usize) -> [u8; 156] {
    let bank = std::fs::read(bank_file_path)
        .unwrap_or_else(|error| {
            eprintln!("failed to load bank: {}", error);
            std::process::exit(1);
        });
    let mut patch;
    if bank.len() == 4104 {
        if !bank.starts_with(header) {
            crate::exit("unrecognized bank format");
        }
        if patch_index >= 32 {
            crate::exit("patch does not exist in bank");
        }
        let patch_offset = header.len() + 128 * patch_index as usize;
        let source = bank[patch_offset..patch_offset + 128].try_into().unwrap();
        patch = crate::unpack::unpack_patch(&source).unwrap();
        eprintln!("patch name: {:?}", std::str::from_utf8(&patch[145..155]).unwrap());
    } else if bank.len() % 145 == 0 {
        let offset = 145 * patch_index;
        let mut source = [0; 156];
        source[..145].copy_from_slice(&bank[offset..offset + 145]);
        patch = source;
        for oscillator_index in 0..6 {
            let value = &mut patch[21 * oscillator_index + 20];
            if *value > 7 {
                *value = 7;
            }
        }
    } else {
        crate::exit("unrecognized bank format");
    }
    patch
}
