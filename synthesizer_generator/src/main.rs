#![allow(non_upper_case_globals)]
#![forbid(non_ascii_idents)]

mod conversions;
mod generate;
mod load;
mod unpack;

fn main() {
    let mut arguments = std::env::args().skip(1);
    let mut bank_file_path = None;
    let mut patch_index = None;
    let mut output_file_path = None;
    while let Some(argument) = arguments.next() {
        match &argument[..] {
            "bank" => {
                if bank_file_path.is_some() {
                    invalid_use();
                }
                bank_file_path = Some(arguments.next().unwrap_or_else(|| invalid_use()));
            }
            "output" => {
                if output_file_path.is_some() {
                    invalid_use();
                }
                output_file_path = Some(arguments.next().unwrap_or_else(|| invalid_use()));
            }
            "patch" => {
                if patch_index.is_some() {
                    invalid_use();
                }
                patch_index = Some(arguments.next()
                    .and_then(|argument| argument.parse().ok())
                    .unwrap_or_else(|| invalid_use()));
            }
            _ => {
                invalid_use();
            }
        }
    }
    let bank_file_path = bank_file_path.unwrap_or_else(|| invalid_use());
    let patch_index = patch_index.unwrap_or_else(|| invalid_use());
    let patch_source = load::load_patch(&bank_file_path, patch_index);
    let output = generate::generate(patch_source);
    match output_file_path {
        None => {
            print!("{}", output);
        }
        Some(output_file_path) => {
            if let Err(error) = std::fs::write(output_file_path, output) {
                eprintln!("failed to write output file: {}", error);
                std::process::exit(1);
            }
        }
    }
}

fn exit(message: &str) -> ! {
    eprintln!("{}", message);
    std::process::exit(1);
}

fn invalid_use() -> ! {
    exit("invalid use");
}
