#![forbid(non_ascii_idents)]

enum Command {
    Default,
    Notes(Vec<(u8, u8)>),
    Random,
    Seed(u64),
}

fn main() {
    let mut arguments = std::env::args().skip(1);
    let mut commands = vec![];
    let mut verbose = false;
    while let Some(argument) = arguments.next() {
        match &argument[..] {
            "default" => {
                commands.push(Command::Default);
            }
            "notes" => {
                let mut notes = vec![];
                while let Some(argument) = arguments.next() {
                    if argument == "-" {
                        break;
                    }
                    let note_value: u8 = argument.parse()
                        .ok()
                        .filter(|&argument| argument < 128)
                        .unwrap_or_else(|| invalid_use());
                    let duration: std::num::NonZeroU8 = arguments.next()
                        .and_then(|argument| argument.parse().ok())
                        .unwrap_or_else(|| invalid_use());
                    notes.push((note_value, duration.get()));
                }
                commands.push(Command::Notes(notes));
            }
            "random" => {
                commands.push(Command::Random);
            }
            "seed" => {
                let seed = arguments.next()
                    .and_then(|argument| argument.parse().ok())
                    .unwrap_or_else(|| invalid_use());
                commands.push(Command::Seed(seed));
            }
            "verbose" => {
                verbose = true;
            }
            _ => {
                invalid_use();
            }
        }
    }
    let initialized = unsafe { fanfare::initialize() };
    for command in commands {
        match command {
            Command::Default => {
                if verbose {
                    eprintln!("default");
                }
                fanfare::play_default(initialized);
            }
            Command::Notes(notes) => {
                if verbose {
                    eprintln!("notes");
                }
                fanfare::play(initialized, &notes);
            }
            Command::Random => {
                let mut rng = <rand::rngs::SmallRng as rand::SeedableRng>::from_entropy();
                let seed = rand::Rng::gen(&mut rng);
                if verbose {
                    eprintln!("random {}", seed);
                }
                fanfare::play(initialized, &fanfare::generate_melody(seed));
            }
            Command::Seed(seed) => {
                if verbose {
                    eprintln!("seed {}", seed);
                }
                fanfare::play(initialized, &fanfare::generate_melody(seed));
            }
        }
    }
}

fn invalid_use() -> ! {
    eprintln!("invalid use");
    std::process::exit(1);
}
