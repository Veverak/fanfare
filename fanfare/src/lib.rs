#![allow(non_upper_case_globals)]
#![forbid(non_ascii_idents)]

use alsa::pcm::State;
pub use synthesizer::{Initialized, initialize};
use rand::Rng;

pub fn generate_melody(seed: u64) -> Vec<(u8, u8)> {
    let mut rng = <rand::rngs::SmallRng as rand::SeedableRng>::seed_from_u64(seed);
    let mut melody = vec![];
    melody.push((notes::C1, 2));
    let note_count = 3;
    let long_note = rng.gen_range(0..note_count);
    let trill_note = rng.gen_range(0..note_count - 1);
    for note_index in 0..note_count {
        let scale = [
            notes::C1,
            notes::D1,
            notes::E1,
            notes::F1,
            notes::G1,
            notes::A1,
            notes::B1,
        ];
        let note = scale[rng.gen_range(0..7)];
        let duration = if note_index == long_note {
            4
        } else if note_index == trill_note {
            melody.push((note - 1, 1));
            1
        } else {
            2
        };
        melody.push((note, duration));
    }
    melody
}

pub fn play(initialized: Initialized, melody: &[(u8, u8)]) {
    let pcm = alsa::PCM::new("default", alsa::Direction::Playback, false).unwrap();
    let parameters = alsa::pcm::HwParams::any(&pcm).unwrap();
    parameters.set_access(alsa::pcm::Access::RWInterleaved).unwrap();
    parameters.set_channels(1).unwrap();
    parameters.set_rate(synthesizer::sample_rate, alsa::ValueOr::Nearest).unwrap();
    parameters.set_format(alsa::pcm::Format::s16()).unwrap();
    pcm.hw_params(&parameters).unwrap();
    let io = pcm.io_i16().unwrap();
    let hw_params = pcm.hw_params_current().unwrap();
    let sw_params = pcm.sw_params_current().unwrap();
    sw_params.set_start_threshold(hw_params.get_buffer_size().unwrap()).unwrap();
    pcm.sw_params(&sw_params).unwrap();
    let mut melody_iterator = melody.iter();
    let mut note_delay = 0;
    let mut notes: Vec<synthesizer::Note> = vec![];
    let mut buffer_offset = synthesizer::buffer_size;
    let mut converted_buffer = [0; synthesizer::buffer_size];
    let mut internal_buffer = [0; synthesizer::buffer_size];
    let mut output_buffer = [0; synthesizer::buffer_size];
    loop {
        if buffer_offset >= synthesizer::buffer_size {
            buffer_offset = 0;
            if note_delay == 0 {
                if let Some(note) = notes.last_mut() {
                    note.key_up();
                }
                if let Some(&(note_value, duration)) = melody_iterator.next() {
                    note_delay = 50 * duration as i32;
                    let mut note = synthesizer::Note::default();
                    note.initialize(initialized, note_value);
                    notes.push(note);
                }
            } else if note_delay <= -350 {
                break;
            }
            note_delay -= 1;
            output_buffer.fill(0);
            for note in &mut notes {
                note.synthesize(initialized, &mut output_buffer, &mut internal_buffer);
            }
            for index in 0..synthesizer::buffer_size {
                converted_buffer[index] = (output_buffer[index] >> 13) as i16;
            }
        }
        let length = io.writei(&converted_buffer[buffer_offset..]).unwrap();
        buffer_offset += length;
    }
    if pcm.state() != State::Running {
        pcm.start().unwrap();
    }
    pcm.drain().unwrap();
}

pub fn play_default(initialized: Initialized) {
    play(initialized, melodies::default);
}

pub mod melodies {
    use crate::notes;

    pub const default: &[(u8, u8)] = &[
        (notes::E1, 1),
        (notes::B1, 1),
    ];

    pub const negative: &[(u8, u8)] = &[
        (notes::C1, 1),
        (notes::E1, 1),
        (notes::C1, 4),
    ];

    pub const positive: &[(u8, u8)] = &[
        (notes::C1, 1),
        (notes::G1, 1),
        (notes::C2, 4),
    ];
}

pub mod notes {
    pub const C1: u8 = 60;
    pub const D1: u8 = 62;
    pub const E1: u8 = 64;
    pub const F1: u8 = 65;
    pub const G1: u8 = 67;
    pub const A1: u8 = 69;
    pub const B1: u8 = 71;
    pub const C2: u8 = 72;
}
